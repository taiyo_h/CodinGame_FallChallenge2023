//
//  main.cpp
//  CodinGame_FallChallenge2023
//
//  Created by 原大曜 on 2023/12/19.
//
#define DEBUG 1

#if DEBUG
#define ASSERT(x) assert(x)
#define LOG(x) std::cerr<<x
#define LOGLN(x) LOG(x)<<endl
#else
#define ASSERT(x)
#define LOG(x)
#define LOGLN(x)
#endif

#include <istream>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <sstream>
#include <array>
#include <optional>
#include <vector>
#include <cmath>
#include <queue>
#include <algorithm>
#include <list>
#include <utility>

using namespace std;

template<typename Container, typename Func>
auto FindIf(const Container& container, Func&& func)->decltype(&(*container.begin())){
    for(const auto& v: container){
        if(func(v)){
            return &v;
        }
    }
    return nullptr;
};

template<typename T, typename Func>
int EraseIf(vector<T>& vec, Func&& func){
    auto it = remove_if(vec.begin(), vec.end(), func);
    int count = vec.end() - it;
    vec.erase(it, vec.end());
    return count;
}

class Input{
public:
    Input(istream& stream):mStream(stream){}
    int nextInt(){int i; this->operator>>(i); return i;}
    void ignore(){mStream.ignore();}
    Input& operator>>(int& n){mStream>>n; LOG("<"<<n);return *this;}
    Input& operator>>(string& s){mStream>>s; LOG("<'"<<s<<"'"); return *this;}
private:
    istream& mStream;
};

//型Tをスコア計算してScore型の値で保持する
//evalで逐次的に評価したスコアを比較し、最も高いスコアをget関数で取得する
template<typename T, typename Score>
class MaxScoreSelection{
public:
    using EvalFunc = std::function<Score(const T&)>; //評価関数の型
    
    MaxScoreSelection(EvalFunc&& func):mEvalFunc(std::forward<EvalFunc>(func)){}

    bool eval(const T& value){
        Score s = mEvalFunc(value);
        if(!mSelected || s > mSelected->second){
            mSelected = {value, std::move(s)};
            return true;
        }
        return false;
    }
    const T* get()const{return mSelected ? &(mSelected->first) : nullptr;}
private:
    optional<pair<T,Score>> mSelected;
    EvalFunc mEvalFunc;
};

template<int Capacity>
class Counter{
public:
    void add(int index, int value = 1){mCount[index]+=value;}
    optional<int> indexOfMax(int minValue = 0)const{
        optional<int> ret;
        for(int i=0;i<mCount.size();i++){
            if(mCount[i] < minValue){continue;}
            if(!ret || mCount[*ret] < mCount[i]){ret = i;}
        }
        return ret;
    }
    optional<int> indexOfMin(int minValue = 0)const{
        optional<int> ret;
        for(int i=0;i<mCount.size();i++){
            if(mCount[i] < minValue){continue;}
            if(!ret || mCount[*ret] > mCount[i]){ret = i;}
        }
        return ret;
    }

private:
    array<int, Capacity> mCount{0};
};

class Vec2{
public:
    constexpr Vec2():Vec2(0,0){}
    constexpr Vec2(int x,int y):mX(x),mY(y){}
    constexpr Vec2(const Vec2& vec):mX(vec.x()),mY(vec.y()){}
    constexpr Vec2 operator+ (const Vec2& r) const {return {x()+r.x(), y()+r.y()};}
    constexpr Vec2 operator- (const Vec2& r) const {return {x()-r.x(), y()-r.y()};}
    constexpr Vec2 operator* (int v) const         {return {x()*v, y()*v};}
    constexpr Vec2 operator/ (int v) const         {return {x()/v, y()/v};}
    constexpr Vec2& operator+= (const Vec2& r) {mX += r.x(); mY+= r.y(); return *this;}
    constexpr bool operator==(const Vec2& r)const{return x() == r.x() && y() == r.y();}
    constexpr bool operator!=(const Vec2& r)const{return !operator==(r);}
    constexpr double length() const{return sqrt((double)mX*mX+(double)mY*mY);}
    constexpr int sqLength() const{return mX*mX+mY*mY;}
    constexpr static Vec2 zero(){return {0,0};}
    string toString() const{return "("+to_string(mX)+","+to_string(mY)+")";}
    constexpr int x() const {return mX;}
    constexpr int y() const {return mY;}
    int& x() {return mX;}
    int& y() {return mY;}
private:
    int mX;
    int mY;
};

class Circle{
public:
    Circle(const Vec2& center, int r)
    :mCenter(center),mRadius(r){}
    
    bool collides(const Circle& other) const{
        int r = mRadius + other.mRadius;
        return (other.mCenter - mCenter).sqLength() <= r * r;
    }
    bool collides(const Vec2& pos) const {
        return collides({pos, 0});
    }

private:
    Vec2 mCenter;
    int mRadius;
};

class Rect{
public:
    //v0,v1を対角とする矩形(位置関係は問わない)
    constexpr Rect(const Vec2& v0, const Vec2& v1)
    :mTopLeft(min(v0.x(), v1.x()), min(v0.y(), v1.y()))
    ,mBottomRight(max(v0.x(), v1.x()), max(v0.y(), v1.y())){}
    
    constexpr int width()  const{return mBottomRight.x() - mTopLeft.x();}
    constexpr int height() const{return mBottomRight.y() - mTopLeft.y();}
    
    constexpr int left() const{return mTopLeft.x();}
    constexpr int right() const{return mBottomRight.x();}
    constexpr int top() const{return mTopLeft.y();}
    constexpr int bottom() const{return mBottomRight.y();}

    //２つの矩形が完全に重複する矩形を計算する
    //存在しないならnullopt
    optional<Rect> overlap(const Rect& other) const{
        int l = max(left(), other.left());
        int r = min(right(), other.right());
        int t = max(top(), other.top());
        int b = min(bottom(), other.bottom());
        if(l > r || t > b){return nullopt;}
        return Rect({l,t},{r,b});
    }
    constexpr Vec2 center() const{return (mTopLeft + mBottomRight)/2;}
    
    bool contains(const Vec2& v) const{
        return v.x() >= left() && v.x() <= right()
        && v.y() <= bottom() && v.y() >= top();
    };
private:
    Vec2 mTopLeft;
    Vec2 mBottomRight;
};

template <typename T, typename Tag>
class NamedType
{
public:
    constexpr explicit NamedType(const T& value) : mValue(value) {}
    T& get() { return mValue; }
    T const& get() const {return mValue; }
    bool operator==(const NamedType& rhs)const{return mValue==rhs.mValue;}
    bool operator!=(const NamedType& rhs)const{return mValue!=rhs.mValue;}
private:
    T mValue;
};

using CreatureId = NamedType<int, class CreatureId_>;
using CreatureType = NamedType<int, class CreatureType_>;
constexpr CreatureType cCreatureType_Monster = CreatureType(-1);

using CreatureColor = NamedType<int, class CreatureColor_>;
using Score = NamedType<int, class Score_>;
using DroneId = NamedType<int, class DroneId_>;
using Emergency = NamedType<int, class Emergency_>;
using Battery = NamedType<int, class Battery_>;

constexpr int cGameTurn = 200;
constexpr int cFieldWidth  = 10000;
constexpr int cFieldHeight = 10000;
constexpr int cFieldRight  = cFieldWidth - 1;
constexpr int cFieldLeft = 0;
constexpr int cFieldTop  = 0;
constexpr int cFieldBottom = cFieldHeight - 1;
constexpr Vec2 cFieldTopLeft     = {0, 0};
constexpr Vec2 cFieldTopRight    = {cFieldWidth-1, 0}; //フィールドの有効領域右上(9999,0)
constexpr Vec2 cFieldBottomLeft  = {0, cFieldHeight-1}; //フィールドの有効領域左下(0,9999)
constexpr Vec2 cFieldBottomRight = {cFieldWidth-1, cFieldHeight-1}; //フィールドの有効領域右下(9999,9999)
constexpr Rect cFieldRect = {cFieldTopLeft, cFieldBottomRight};

struct CreatureData{
    int mMinY; //生息域最小値
    int mMaxY; //生息地最大値
    Score mPoint; //スキャン時のスコア。初スキャンの場合は2倍。
    Rect havitatRect()const{return {{0, mMinY}, {cFieldWidth - 1, mMaxY}};}
};
/*
struct MonsterData{
    int mMinY; //生息域最小値
    int mMaxY; //生息地最大値
};
 */
const CreatureData& GetCreatureData(CreatureType type){
    static CreatureData sFishData[3] = {
        {2500, 5000, Score(1)},
        {5000, 7500, Score(2)},
        {7500, 10000, Score(3)}
    };
    static CreatureData sMonsterData = {
        2500, 10000, Score(-1)
    };
    if(type == cCreatureType_Monster){return sMonsterData;}
    return sFishData[type.get()];
}


struct Motion{
    Vec2 mPosition;
    Vec2 mVelocity;
};

struct CreatureProfile{
    CreatureId mId;
    CreatureType mType;
    CreatureColor mColor;
};

enum class ScanType{
    None,       //ScanもDroneScanもされていない
    Scan,       //Scanしてスコア化済み
    DroneScan   //Scanしているがまだスコア化されていない
};

struct VisibleCreature{
    CreatureId mId;
    Motion mMotion;
};

struct Drone{
    DroneId mId;
    Vec2 mPos;
    Emergency mEmergency;
    Battery mBattery;
};

//各ドローンがスキャンしてまだセーブはしていないクリーチャー
struct DroneScan{
    DroneId mDroneId;
    CreatureId mCreatureId;
};

struct RadarBlip{
    DroneId mDroneId;
    CreatureId mCreatureId;
    string mRadar;
};

enum class RectCornerType : int {
    TL=0, TR, BR, BL
};
struct RectCorner{
    RectCornerType mType;
    string mName;
    Vec2   mDir;
};
const RectCorner cTL = {RectCornerType::TL, "TL", {-1,-1}};
const RectCorner cTR = {RectCornerType::TR, "TR", { 1,-1}};
const RectCorner cBR = {RectCornerType::BR, "BR", { 1, 1}};
const RectCorner cBL = {RectCornerType::BL, "BL", {-1, 1}};
const array<RectCorner,4> cCorner4 = {
    cTL,
    cTR,
    cBR,
    cBL
};
const RectCorner& FindRectCorner(string name){
    for(const auto& corner: cCorner4){
        if(name == corner.mName){return corner;}
    }
    ASSERT(false);
    return cTL;
}
const RectCorner& GetRectCorner(RectCornerType type){
    return cCorner4[static_cast<int>(type)];
}

class DirectionalPoint{
public:
    DirectionalPoint(const Vec2& basePoint, const RectCorner& dir) : mBasePoint(basePoint), mDir(dir){};
    //BaseからDir方向に半直線を伸ばし、指定した矩形内かつ直線上のある座標を返す
    //座標は、次の2つのどちらかで求める
    //Baseが矩形内の場合: 半直線が矩形の縁と衝突する座標と、Baseの中間地点(本当はLightで見えている場所は除外する必要がある...)
    //Baseが矩形外の場合: 半直線が矩形の縁と2回衝突するときの、その2箇所の中間地点
    Vec2 roughPosition() const {return mBasePoint + mDir.mDir * 100000;}
    
    const RectCorner& dir() const{return mDir;}
    const Vec2& basePoint() const{return mBasePoint;}
    
private:
    Vec2 mBasePoint;
    RectCorner mDir;
};

class MotionResolver{
private:
    //ターン毎のMotion情報
    struct TurnInfo{
        vector<DirectionalPoint> mBlips;
        optional<Motion> mMotion;
        optional<Rect> blipsOverlaped(const Rect& havitatRect) const{
            if(mBlips.empty()){ return nullopt;}
            Rect r = cFieldRect;
            for(const DirectionalPoint& dp: mBlips){
                r = *r.overlap(locatbleRect(havitatRect, dp));
            }
            return r;
        }
    private:
        //生息領域と観測情報から、存在しうる矩形を求める
        Rect locatbleRect(const Rect& havitatRect, const DirectionalPoint& dp) const{
            Rect r = havitatRect;
            switch (dp.dir().mType) {
                case RectCornerType::TL: return *r.overlap(Rect(dp.basePoint(), cFieldTopLeft));
                case RectCornerType::TR: return *r.overlap(Rect(dp.basePoint(), cFieldTopRight));
                case RectCornerType::BL: return *r.overlap(Rect(dp.basePoint(), cFieldBottomLeft));
                case RectCornerType::BR: return *r.overlap(Rect(dp.basePoint(), cFieldBottomRight));
            }
            ASSERT(false);
            return Rect(Vec2::zero(), Vec2::zero());
        }
    };
public:
    MotionResolver(CreatureType type):mType(type){
        mTurnInfoList.emplace_back();
    }
    //ターン毎のBlipsの情報を渡す(Blipsの情報がない場合は呼ばなくていい)
    void add(const Vec2& dronePos, const RectCorner& corner){
        mTurnInfoList.back().mBlips.push_back({dronePos, corner});
    }
    //ターンごとの正確なMotionを渡す(正確な位置情報がない場合は呼ばなくていい)
    void setMotion(const Motion& motion){
        mTurnInfoList.back().mMotion = motion;
    }
    
    void update(){
        const auto latest = mTurnInfoList.back();
        if(latest.mBlips.empty()){ //追い出され済み or なにかロジックエラー
            mEstimatedPosition.reset();
            mVisibleMotion.reset();
            mTurnInfoList.emplace_back();
            return;
        }
        if(latest.mMotion){
            //最新かつ正確な情報がある
            mVisibleMotion = latest.mMotion;
            mEstimatedPosition = latest.mMotion->mPosition;
            mTurnInfoList.emplace_back();
            return;
        }
        const CreatureData& data = GetCreatureData(mType);
        
        //正確な情報はないので、仮想の位置を使って加重平均
        Vec2 pos = latest.blipsOverlaped(data.havitatRect())->center();
        if(mEstimatedPosition){
            mEstimatedPosition = pos/2 + *mEstimatedPosition/2;
        } else {
            mEstimatedPosition = pos;
        }
        mVisibleMotion = nullopt;
        mTurnInfoList.emplace_back();
    }
    optional<Vec2> estimatedPos() const{
        return mEstimatedPosition;
    }
    const optional<Motion>& visibleMotion() const{
        return mVisibleMotion;
    }
private:
    CreatureType mType;
    optional<Vec2> mEstimatedPosition;
    optional<Motion> mVisibleMotion;
    
    vector<TurnInfo> mTurnInfoList; //レーダー情報、foeのレーダーは入力できないため、必然的にmyの情報しか入らない。ターン毎に対応するindexに保持。
};

class Creature{
public:
    Creature(const CreatureProfile& profile)
    :mProfile(profile),mMotionResolver(profile.mType){}
    
    void setVisibleMotion(const Motion& motion){mMotionResolver.setMotion(motion);}
    
    void setScanTypeForMe(ScanType forMe)      {mScanTypeForMe = forMe;}
    void setScanTypeForFoe(ScanType forFoe)    {mScanTypeForFoe = forFoe;}

    void addMyRadarBlip(const Vec2& dronePos, const RectCorner& corner){
        mMotionResolver.add(dronePos, corner);
    }
    void update(){
        mMotionResolver.update();
    }
     
    CreatureId id()           const{return mProfile.mId;}
    CreatureType type()       const{return mProfile.mType;}
    bool isFish()             const{return type().get() >= 0;}
    bool isMonster()          const{return type() == CreatureType(-1);}
    CreatureColor color()     const{return mProfile.mColor;}
    ScanType scanTypeForMe()  const{return mScanTypeForMe;}
    bool isScanTypeNoneForMe() const{return scanTypeForMe() == ScanType::None;}
    ScanType scanTypeForFoe() const{return mScanTypeForFoe;}
    const optional<Motion>&  visibleMotion() const{return mMotionResolver.visibleMotion();}
    optional<Vec2> estimatedPos() const{
        return mMotionResolver.estimatedPos();
    }
    optional<Vec2> bestEffortPos() const{
        if(auto vm = visibleMotion()){return vm->mPosition;}
        return estimatedPos();
    }

    
private:
    CreatureProfile mProfile;
    ScanType mScanTypeForMe = ScanType::None;
    ScanType mScanTypeForFoe = ScanType::None;
    MotionResolver mMotionResolver;
};


class Player{
public:
    Player():mScore(0){
    }
    void setScore(Score score){mScore = score;}
    void setScans(const vector<CreatureId>& scans){mScans = scans;}
    void updateDrone(DroneId droneId, const Vec2& pos, Emergency emergency, Battery battery){
        if(Drone* pDrone = findDrone_(droneId)){
            pDrone->mId = droneId; pDrone->mPos = pos; pDrone->mEmergency = emergency; pDrone->mBattery = battery;
            return;
        }
        mDrones.push_back(Drone{droneId,pos,emergency,battery});
    }
    
    
    const Drone* findDrone(DroneId id) const { return findDroneIf([id](const Drone& drone){return drone.mId == id;});}
    template<typename Func>
    const Drone* findDroneIf(Func&& func)const{
        for(const Drone& drone: mDrones){if(func(drone)){return &drone;}}
        return nullptr;
    }
    const Drone& getFirstDrone()const{return getDrone(0);}
    const Drone& getDrone(int idx)const{return mDrones[idx];}

    bool containsScaned(CreatureId id) const{return find(mScans.begin(), mScans.end(), id) != mScans.end();}
private:
    Drone* findDrone_(DroneId id) {return const_cast<Drone*>(as_const(*this).findDrone(id));}

    Score mScore;
    vector<CreatureId> mScans; //スコア化済みのクリーチャー
    vector<Drone> mDrones;
};

class PlayField{
public:
    PlayField(const vector<CreatureProfile>& profiles)
    : mCreatureProfiles(profiles)
    {}
    
    Player* getMyPlayer(){return &mMyPlayer;}
    Player* getFoePlayer(){return &mFoePlayer;}
    void setDroneScans(const vector<DroneScan>& droneScans){mDroneScans = droneScans;}
    void setVisibleCreatures(const vector<VisibleCreature>& visibleCreatures){mVisibleCreatures = visibleCreatures;}
    void setRadarBlips(const vector<RadarBlip> radarBlips){mMyRadarBlips = radarBlips;}
    //各set関数で設定した情報をもとに最新のCreatures情報を更新する
    void updateCreatures(){
        for(const CreatureProfile& profile: mCreatureProfiles){
            Creature* pCreature = findCreature_(profile.mId);
            if(!pCreature){
                pCreature = &mCreatures.emplace_back(profile);
            }
            if(const VisibleCreature* pVisibleCreature = FindIf(mVisibleCreatures, [id=profile.mId](const VisibleCreature& vc){
                return vc.mId == id;
            })){
                pCreature->setVisibleMotion(pVisibleCreature->mMotion);
            }
            pCreature->setScanTypeForMe(ScanType::None);
            pCreature->setScanTypeForFoe(ScanType::None);
            for(const DroneScan& ds: mDroneScans){
                if(ds.mCreatureId == pCreature->id()){
                    if(mMyPlayer.findDrone(ds.mDroneId)){
                        pCreature->setScanTypeForMe(ScanType::DroneScan);
                    }
                    if(mFoePlayer.findDrone(ds.mDroneId)){
                        pCreature->setScanTypeForFoe(ScanType::DroneScan);
                    }
                }
            }
            if(mMyPlayer.containsScaned(pCreature->id())){
                pCreature->setScanTypeForMe(ScanType::Scan);
            }
            if(mFoePlayer.containsScaned(pCreature->id())){
                pCreature->setScanTypeForFoe(ScanType::Scan);
            }
            for(const RadarBlip& rb: mMyRadarBlips){
                if(rb.mCreatureId == pCreature->id()){
                    pCreature->addMyRadarBlip(findMyDrone(rb.mDroneId)->mPos, FindRectCorner(rb.mRadar));
                }
            }
            pCreature->update();
            LOG("Creature " << pCreature->id().get()
                << ", visiblePos:" << (pCreature->visibleMotion()?pCreature->visibleMotion()->mPosition.toString():"unknown")
                << ", estimatedPos:" << (pCreature->estimatedPos()?pCreature->estimatedPos()->toString():"unknown")) << endl;
        }
    }
    
    const Drone& getMyDrone(int idx)const{return mMyPlayer.getDrone(idx);}

    const Drone* findMyDrone(DroneId id) const{ return mMyPlayer.findDrone(id);}

    template<typename Func>
    const Creature* findCreatureIf(Func&& func) const{
        for(const Creature& c: mCreatures){if(func(c)){return &c;}}
        return nullptr;
    }

    template<typename Func>
    const Creature* findMonsterIf(Func&& func) const{
        return findCreatureIf([&func](const Creature& c){
            return c.isMonster() && func(c);
        });
    }

    const Creature* findCreature(CreatureId id) const{return findCreatureIf([id](const Creature& c){return c.id() == id;});}

    template<typename Func>
    void forEachCreature(Func&& func) const{
        findCreatureIf([&func](const Creature& c){
            func(c); 
            return false;
        });
    }

    template<typename Func>
    void forEachMonster(Func&& func) const{
        findMonsterIf([&func](const Creature& c){
            func(c);
            return false;
        });
    }

    template<typename Func>
    const DroneScan* findDroneScanIf(Func&& func) const{
        for(const DroneScan& ds: mDroneScans){ if(func(ds)){return &ds;} }
        return nullptr;
    }
    //droneIdが指すDroneが保持するDroneScanをfindIfする
    template<typename Func>
    const DroneScan* findDroneScanIf(DroneId droneId, Func&& func) const {
        return findDroneScanIf([droneId, &func](const DroneScan& ds){return ds.mDroneId == droneId && func(ds);});
    }
    //droneIdが指すDroneが保持する最初のDroneScanを返す。複数ある場合、最初の1つになる。
    const DroneScan* findDroneScan(DroneId droneId) const{ return findDroneScanIf([droneId](const DroneScan& ds){return ds.mDroneId == droneId;}); }
    //droneIdが指すDroneが保持するcreatureIdを対象としたDroneScaneを返す。
    const DroneScan* findDroneScan(DroneId droneId, CreatureId creatureId) const{ return findDroneScanIf([droneId, creatureId](const DroneScan& ds){return ds.mDroneId == droneId && ds.mCreatureId == creatureId;}); }

private:
    Creature* findCreature_(CreatureId id){
        return const_cast<Creature*>(findCreature(id));
    }

    Player mMyPlayer;
    Player mFoePlayer;
    vector<DroneScan> mDroneScans; //スキャン済みだが、まだセーブできていないもの。敵の情報も味方の情報も両方入る。
    vector<RadarBlip> mMyRadarBlips; //自分のドローンに対して、クリーチャーのいちを大まかに知ることができる
    vector<Creature> mCreatures;
    //以下は構築時に設定するが、Creatureに統合し、直接外からは参照しないようにするつもり
    vector<CreatureProfile> mCreatureProfiles;
    vector<VisibleCreature> mVisibleCreatures;
};

class PlayFieldUpdater{
public:
    PlayFieldUpdater(istream& is):mInput(is){
        int count = mInput.nextInt(); mInput.ignore();
        vector<CreatureProfile> profiles;
        for(int i=0;i<count;i++){
            int id, color, type;
            mInput >> id >> color >> type; mInput.ignore();
            profiles.emplace_back(CreatureProfile{CreatureId(id), CreatureType(type), CreatureColor(color)});
        }
        mPlayField.emplace(profiles);
    }
    
    //1ターン分の入力からビルド
    const PlayField& update(){
        Player* pMyPlayer = mPlayField->getMyPlayer();
        Player* pFoePlayer = mPlayField->getFoePlayer();
        LOGLN("\nScore");
        int myScore = mInput.nextInt(); mInput.ignore();
        int foeScore = mInput.nextInt(); mInput.ignore();
        pMyPlayer->setScore(Score(myScore));
        pFoePlayer->setScore(Score(foeScore));
        LOGLN("\nScans");
        pMyPlayer->setScans(buildScans());
        pFoePlayer->setScans(buildScans());
        LOGLN("\nDrones");
        for(const Drone& d: buildDrones()){ pMyPlayer->updateDrone(d.mId, d.mPos, d.mEmergency, d.mBattery); }
        for(const Drone& d: buildDrones()){ pFoePlayer->updateDrone(d.mId, d.mPos, d.mEmergency, d.mBattery); }
        LOGLN("\nDroneScans");
        mPlayField->setDroneScans(buildDroneScans());
        LOGLN("\nVisibleCreature");
        mPlayField->setVisibleCreatures(buildVisibleCreatures());
        LOGLN("\nBlips");
        mPlayField->setRadarBlips(buildRadarBlips());
        LOGLN("\nupdateCreatures");
        mPlayField->updateCreatures();
        LOGLN("\nDone");
        
        return *mPlayField;
    }
private:
    vector<CreatureId> buildScans(){
        vector<CreatureId> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id;
            mInput >> id; mInput.ignore();
            ret.emplace_back(CreatureId(id));
        }
        return ret;
    }
    vector<Drone> buildDrones(){
        vector<Drone> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id, x, y, emergency, battery;
            mInput >> id >> x >> y >> emergency >> battery; mInput.ignore();
            ret.emplace_back(Drone{DroneId(id), Vec2(x,y), Emergency(emergency), Battery(battery)});
        }
        return ret;
    }
    vector<DroneScan> buildDroneScans(){
        vector<DroneScan> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int droneId, creatureId;
            mInput >> droneId >> creatureId; mInput.ignore();
            ret.emplace_back(DroneScan{DroneId(droneId), CreatureId(creatureId)});
        }
        return ret;
    }
    vector<VisibleCreature> buildVisibleCreatures(){
        vector<VisibleCreature> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id, x, y, vx, vy;
            mInput >> id >> x >> y >> vx >> vy; mInput.ignore();
            ret.emplace_back(VisibleCreature{CreatureId(id), Motion{Vec2(x, y), Vec2(vx, vy)}});
        }
        return ret;
    }
    vector<RadarBlip> buildRadarBlips(){
        vector<RadarBlip> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int droneId, creatureId;
            string radar;
            mInput >> droneId >> creatureId >> radar; mInput.ignore();
            ret.emplace_back(RadarBlip{DroneId(droneId), CreatureId(creatureId), radar});
        }
        return ret;
    }

    optional<PlayField> mPlayField;
    Input mInput;
};

class Command{
public:
    void setMove(const Vec2& to){
        mMoveTo=to;
    }
    void enablePowerLight(){mIsEnablePowerLight=true;}
    string toString() const{
        stringstream ss;
        if(mMoveTo){ ss << "MOVE " << mMoveTo->x() << " " << mMoveTo->y();}
        else {ss << "WAIT";}
        ss << " " << (mIsEnablePowerLight ? "1" : "0");
        return ss.str();
    }
private:
    optional<Vec2> mMoveTo; //有効ならMOVE、そうでなければWAIT
    bool mIsEnablePowerLight = false;
};

//各階層の魚で、まだScanもDroneScanもできていないものを探し、その位置を返す(位置は推定している場合もある)
template<typename Func>
const Creature* FindUnscanedCreatureIf(const PlayField& pf, Func&& func){
    return pf.findCreatureIf([&func](const Creature& c){
        return c.isScanTypeNoneForMe() && func(c);
    });
}

//各階層の魚で、まだScanもDroneScanもできていないものを探し、その位置を返す(位置は推定している場合もある)
const Creature* FindUnscanedCreature(const PlayField& pf, CreatureType type){
    return FindUnscanedCreatureIf(pf, [type](const Creature& c){
        return c.type() == type;
    });
}
optional<Vec2> FindUnscanedFishPos(const PlayField& pf){
    for(int type=0;type<=2;type++){
        if(const Creature* pCreature = FindUnscanedCreature(pf, CreatureType(type))){
            return pCreature->bestEffortPos();
        }
    }
    return nullopt;
}
const Creature* MyNearestUnscanedVisibleFish(const PlayField& pf, const Drone& drone)
{
    MaxScoreSelection<const Creature*, double> selection([&drone](const Creature* c){
        return -(drone.mPos - c->visibleMotion()->mPosition).length();
    });
    pf.forEachCreature([&selection](const Creature& c){
        if(!c.isFish()){return;}
        if(c.scanTypeForMe()!=ScanType::None){return;}
        if(!c.visibleMotion()){return;}
        selection.eval(&c);
    });
    return selection.get() ? *selection.get() : nullptr;
}

//見えているモンスターの次の座標から考えて、衝突が回避できる座標を返す
//モンスターが見えているのは1引きだけとも限らない
optional<Vec2> MovePosForAvoidanceVisibleMonster(const PlayField& pf, const Drone& drone){
    vector<Vec2> monsterPoss;
    pf.forEachMonster([&monsterPoss,&drone](const Creature& c){
        if(c.visibleMotion()){
            //実際にはマップの端とか生息域を考慮する必要がありそうだけどとりあえず
            monsterPoss.push_back(c.visibleMotion()->mPosition + c.visibleMotion()->mVelocity);
        }else if(auto estimatedPos = c.estimatedPos()){
            monsterPoss.push_back(*estimatedPos + ((drone.mPos - *estimatedPos) * 270 / (drone.mPos - *estimatedPos).length())); //モンスターはdroneの方向に向かうと仮定する
        }
    });
    //移動した場所によっては衝突する可能性がある敵の座標だけ残す
    EraseIf(monsterPoss, [&drone](const Vec2& pos){
        return !Circle(pos, 500).collides(Circle(drone.mPos, 600));
    });
    if(monsterPoss.empty()){
        //回避不要
        return nullopt;
    }
    //まだ考慮が抜けている部分が多いが、各モンスターの移動後の位置と、現在のドローンの位置との相対方向をベクトルとし、その和の逆向きへ行く
    Vec2 dirSum(0, 0);
    for(const auto& pos: monsterPoss){
        dirSum += drone.mPos - pos;
    };
    Vec2 tar = drone.mPos + dirSum * 600;
    //もしこの座標が領域外の場合、目一杯逃げられるようにベクトルを少し曲げる
    if(tar.x() < 0 || tar.x() >= cFieldRight){
        tar.x() = min(cFieldRight, max(cFieldLeft, tar.x()));
        if(dirSum.y() <= 0){
            //上に曲げる
            for(int y = tar.y();;y--){
                tar.y() = y;
                if((tar - drone.mPos).length() >= 600){
                    return tar;
                }
            }
        }
        else if(dirSum.y() > 0)
        {
            //下に曲げる
            for(int y = tar.y();;y++){
                tar.y() = y;
                if((tar - drone.mPos).length() >= 600){
                    return tar;
                }
            }
        }
    } else if(tar.y() < 0 || tar.y() >= cFieldBottom){
        tar.x() = min(cFieldBottom, max(cFieldTop, tar.y()));
        if(dirSum.x() <= 0){
            //右に曲げる
            for(int x = tar.x();;x++){
                tar.x() = x;
                if((tar - drone.mPos).length() >= 600){
                    return tar;
                }
            }
        }
        else if(dirSum.x() > 0)
        {
            //左に曲げる
            for(int x = tar.x();;x++){
                tar.x() = x;
                if((tar - drone.mPos).length() >= 600){
                    return tar;
                }
            }
        }
    }
    return tar;
}


int main(int argc, const char * argv[]) {
    PlayFieldUpdater updater(cin);
    
    bool needAvoidanceResurface[2] = {false, false};
    while(true){
        const PlayField& pf = updater.update();
        for(int i=0;i<2;i++){
            const Drone& myDrone = pf.getMyDrone(i);
            Command cmd;
            if(!pf.findDroneScan(myDrone.mId)){
                needAvoidanceResurface[i] = false;
            }
            if(auto avoidanceMovePos = MovePosForAvoidanceVisibleMonster(pf, myDrone)){
                cmd.setMove(*avoidanceMovePos);
                LOGLN("Avoidance " << avoidanceMovePos->toString());
                needAvoidanceResurface[i] = true;
            } else if(needAvoidanceResurface[i] && pf.findDroneScan(myDrone.mId)){
                cmd.setMove({myDrone.mPos.x(), 500});
                LOGLN("AvoidanceResurface");
            } else if(const Creature* pCreature = MyNearestUnscanedVisibleFish(pf, myDrone)){
                cmd.setMove(pCreature->visibleMotion()->mPosition);
                LOGLN("Visible " << pCreature->id().get());
            } else if(const Creature* pCreature0 = FindUnscanedCreatureIf(pf, [&myDrone,&pf, i](const Creature& c){
                if(c.isMonster()){return false;}
                if(!c.bestEffortPos()){return false;}
                if(i == 0){
                    return c.bestEffortPos()->x() <= 5000;
                }else{
                    return c.bestEffortPos()->x() >= 5000;
                }
            })){
                cmd.setMove(*pCreature0->bestEffortPos());
                LOGLN("Unscan1 " << pCreature0->id().get() << "(" << pCreature0->bestEffortPos()->toString() << ")");
            } else if (const auto* pCreature1 = FindUnscanedCreatureIf(pf, [](const Creature& c){
                return c.isFish() && c.bestEffortPos();
            })) {
                cmd.setMove(*pCreature1->bestEffortPos());
                LOGLN("Unscan2 " << pCreature1->id().get() << "(" << pCreature1->bestEffortPos()->toString() << ")");
            } else{
                cmd.setMove({myDrone.mPos.x(), 500});
                LOGLN("Resurface");
            }
            
            if(myDrone.mBattery.get() >= 5){
                cmd.enablePowerLight();
                LOGLN("PowerLight");
            }
            cout << cmd.toString() << endl;
        }
    }
    
    return 0;
}
