//
//  main.cpp
//  CodinGame_FallChallenge2023
//
//  Created by 原大曜 on 2023/12/19.
//
#define DEBUG 0

#if DEBUG
#define ASSERT(x) assert(x)
#define LOG(x) std::cerr<<x
#define LOGLN(x) LOG(x)<<endl
#else
#define ASSERT(x)
#define LOG(x)
#define LOGLN(x)
#endif

#include <istream>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <sstream>
#include <array>
#include <optional>
#include <vector>
#include <cmath>
#include <queue>
#include <algorithm>
#include <list>
#include <utility>
#include <variant>

using namespace std;

//const版 find_if
template<typename Container, typename Func>
auto FindIf(const Container& container, Func&& func)->decltype(&(*container.begin())){
    for(const auto& v: container){
        if(func(v)){
            return &v;
        }
    }
    return nullptr;
};
//非const版 find_if
template<typename Container, typename Func>
auto FindIf(Container& container, Func&& func)->decltype(&(*container.begin())){
    for(auto& v: container){
        if(func(v)){
            return &v;
        }
    }
    return nullptr;
};

//条件を満たす要素を「すべて」削除する。削除した個数を返す。
template<typename T, typename Func>
int EraseIf(vector<T>& vec, Func&& func){
    auto it = remove_if(vec.begin(), vec.end(), func);
    int count = (int)(vec.end() - it);
    vec.erase(it, vec.end());
    return count;
}

class Input{
public:
    Input(istream& stream):mStream(stream){}
    int nextInt(){int i; this->operator>>(i); return i;}
    void ignore(){mStream.ignore();}
    Input& operator>>(int& n){mStream>>n; LOG("<"<<n);return *this;}
    Input& operator>>(string& s){mStream>>s; LOG("<'"<<s<<"'"); return *this;}
private:
    istream& mStream;
};

//型Tをスコア計算してScore型の値で保持する
//evalで逐次的に評価したスコアを比較し、最も高いスコアをget関数で取得する
template<typename T, typename Score>
class MaxScoreSelection{
public:
    using EvalFunc = std::function<Score(const T&)>; //評価関数の型
    
    MaxScoreSelection(EvalFunc&& func):mEvalFunc(std::forward<EvalFunc>(func)){}

    bool eval(const T& value){
        Score s = mEvalFunc(value);
        if(!mSelected || s > mSelected->second){
            mSelected = {value, std::move(s)};
            return true;
        }
        return false;
    }
    const T* get()const{return mSelected ? &(mSelected->first) : nullptr;}
private:
    optional<pair<T,Score>> mSelected;
    EvalFunc mEvalFunc;
};

template<int Capacity>
class Counter{
public:
    void add(int index, int value = 1){mCount[index]+=value;}
    optional<int> indexOfMax(int minValue = 0)const{
        optional<int> ret;
        for(int i=0;i<mCount.size();i++){
            if(mCount[i] < minValue){continue;}
            if(!ret || mCount[*ret] < mCount[i]){ret = i;}
        }
        return ret;
    }
    optional<int> indexOfMin(int minValue = 0)const{
        optional<int> ret;
        for(int i=0;i<mCount.size();i++){
            if(mCount[i] < minValue){continue;}
            if(!ret || mCount[*ret] > mCount[i]){ret = i;}
        }
        return ret;
    }

private:
    array<int, Capacity> mCount{0};
};



template<typename T>
class Vec2{
public:
    constexpr Vec2():Vec2(0,0){}
    constexpr Vec2(T x,T y):mX(x),mY(y){}
    constexpr Vec2(const Vec2& vec):mX(vec.x()),mY(vec.y()){}
    template<typename T2>
    constexpr Vec2(const Vec2<T2>& vec):mX(static_cast<T>(vec.x())),mY(static_cast<T>(vec.y())){}
    
    constexpr Vec2 operator+ (const Vec2& r) const {return {x()+r.x(), y()+r.y()};}
    constexpr Vec2 operator- (const Vec2& r) const {return {x()-r.x(), y()-r.y()};}
    constexpr Vec2 operator* (T v) const         {return {x()*v, y()*v};}
    constexpr Vec2 operator/ (T v) const         {return {x()/v, y()/v};}
    constexpr Vec2& operator+= (const Vec2& r) {mX += r.x(); mY+= r.y(); return *this;}
    constexpr bool operator==(const Vec2& r)const{return x() == r.x() && y() == r.y();}
    constexpr bool operator!=(const Vec2& r)const{return !operator==(r);}
    constexpr T length() const{return sqrt(mX*mX+mY*mY);}
    constexpr T sqLength() const{return mX*mX+mY*mY;}
    //thisとotherがlen距離内にあるかどうかを判定する。lenと距離が等しい場合はtrueを返す
    constexpr bool isInRange(const Vec2& other, T len) const{return (*this-other).sqLength() <= len*len;}
    constexpr static Vec2 zero(){return {0,0};}
    constexpr bool isZero() const{return mX==0&&mY==0;}
    constexpr Vec2 normalize(int length = 1) const{
        ASSERT(*this != zero());
        T x = mX;
        T y = mY;
        T len = this->length();
        x *= length; x /= len;
        y *= length; y /= len;
        return {x, y};
    }
    constexpr Vec2 round() const{
        if constexpr(is_same_v<T, float>||is_same_v<T, double>){
            return {::round(mX), ::round(mY)};
        } else {
            return *this;
        }
    }
    string toString() const{return "("+to_string(mX)+","+to_string(mY)+")";}
    constexpr T x() const {return mX;}
    constexpr T y() const {return mY;}
    T& x() {return mX;}
    T& y() {return mY;}
private:
    T mX;
    T mY;
};

using V2i = Vec2<int>;
using V2d = Vec2<double>;


template<typename T>
class Circle{
public:
    Circle(const Vec2<T>& center, T r)
    :mCenter(center),mRadius(r){}
    
    bool collides(const Circle& other) const{
        int r = mRadius + other.mRadius;
        return (other.mCenter - mCenter).sqLength() <= r * r;
    }
    template<typename VecType>
    bool collides(const Vec2<VecType>& pos) const {
        return collides({pos, 0});
    }
    
    const Vec2<T>& center()const{return mCenter;}

private:
    Vec2<T> mCenter;
    T mRadius;
};

class Rect{
public:
    //v0,v1を対角とする矩形(位置関係は問わない)
    constexpr Rect(const V2i& v0, const V2i& v1)
    :mTopLeft(min(v0.x(), v1.x()), min(v0.y(), v1.y()))
    ,mBottomRight(max(v0.x(), v1.x()), max(v0.y(), v1.y())){}
    
    constexpr int width()  const{return mBottomRight.x() - mTopLeft.x();}
    constexpr int height() const{return mBottomRight.y() - mTopLeft.y();}
    
    constexpr int left() const{return mTopLeft.x();}
    constexpr int right() const{return mBottomRight.x();}
    constexpr int top() const{return mTopLeft.y();}
    constexpr int bottom() const{return mBottomRight.y();}
    
    constexpr V2i topLeft() const{return mTopLeft;}
    constexpr V2i topRight() const{return {right(), top()};}
    constexpr V2i bottomLeft() const{return {left(), bottom()};}
    constexpr V2i bottomRight() const{return mBottomRight;}
    
    constexpr V2i topCenter()    const {return {(right()+left())/2, top()};}
    constexpr V2i bottomCenter() const {return {(right()+left())/2, bottom()};}
    constexpr V2i centerRight()  const {return {right(), (top()+bottom())/2};}
    constexpr V2i centerLeft()   const {return {left() , (top()+bottom())/2};}
    
    constexpr int area() const{return width() * height();}

    //２つの矩形が完全に重複する矩形を計算する
    //存在しないならnullopt
    optional<Rect> overlap(const Rect& other) const{
        int l = max(left(), other.left());
        int r = min(right(), other.right());
        int t = max(top(), other.top());
        int b = min(bottom(), other.bottom());
        if(l > r || t > b){return nullopt;}
        return Rect({l,t},{r,b});
    }
    constexpr V2i center() const{return (mTopLeft + mBottomRight)/2;}
    
    bool contains(const V2i& v) const{
        return v.x() >= left() && v.x() <= right()
        && v.y() <= bottom() && v.y() >= top();
    };
    string toString() const{
        stringstream ss;
        ss<<"{TL:"<<topLeft().toString()<<",BR:"<<bottomRight().toString()<<"}";
        return ss.str();
    }
private:
    V2i mTopLeft;
    V2i mBottomRight;
};

class Closest{
public:
    Closest(const V2d& basePos):mBasePos(basePos){}
    bool eval(const V2d& pos){
        double sqLength = (mBasePos - pos).sqLength();
        if(sqLength > mClosestSqLength){
            return false;
        }
        if(sqLength < mClosestSqLength){
            mClosestPosList.clear();
            mClosestSqLength = sqLength;
        }
        ASSERT(sqLength <= mClosestSqLength); //等しい場合もリストに追加するので許容
        mClosestPosList.push_back(pos);
        return true;
    }
    int sqLength() const{return mClosestSqLength;}
    int length() const{return sqrt(sqLength());}
    V2d meanPos()const{
        ASSERT(!mClosestPosList.empty());
        V2i sum = V2i::zero();
        for(const V2d& pos: mClosestPosList){
            sum += pos;
        }
        return sum / (int)mClosestPosList.size();
    }
    bool empty(){return mClosestPosList.empty();}
    
private:
    const V2d mBasePos;
    vector<V2d> mClosestPosList;
    double mClosestSqLength = numeric_limits<double>::infinity();
};

template <typename T, typename Tag>
class NamedType
{
public:
    constexpr explicit NamedType(const T& value) : mValue(value) {}
    T& get() { return mValue; }
    constexpr const T& get() const {return mValue; }
    constexpr bool       operator==(const NamedType& rhs)const{return mValue==rhs.mValue;}
    constexpr bool       operator!=(const NamedType& rhs)const{return mValue!=rhs.mValue;}
    constexpr bool       operator> (const NamedType& rhs)const{return mValue>rhs.mValue;}
    constexpr bool       operator>=(const NamedType& rhs)const{return mValue>=rhs.mValue;}
    constexpr bool       operator< (const NamedType& rhs)const{return mValue<rhs.mValue;}
    constexpr bool       operator<=(const NamedType& rhs)const{return mValue<=rhs.mValue;}
    constexpr NamedType& operator+=(const NamedType& rhs){mValue += rhs.mValue; return *this;}
    constexpr NamedType& operator-=(const NamedType& rhs){mValue -= rhs.mValue; return *this;}
    constexpr NamedType  operator+(const NamedType& rhs){return NamedType{mValue + rhs.mValue};}
    constexpr NamedType  operator-(const NamedType& rhs){return NamedType{mValue - rhs.mValue};}
private:
    T mValue;
};

template <typename T, typename Tag>
ostream& operator<<(ostream& os, const NamedType<T, Tag>& in){
    os << in.get();
    return os;
}


using CreatureId = NamedType<int, class CreatureId_>;
using CreatureType = NamedType<int, class CreatureType_>;
constexpr CreatureType cCreatureType_Monster = CreatureType(-1);

using CreatureColor = NamedType<int, class CreatureColor_>;
using Score = NamedType<int, class Score_>;
using DroneId = NamedType<int, class DroneId_>;
using Emergency = NamedType<int, class Emergency_>;
using Battery = NamedType<int, class Battery_>;
using Turn = NamedType<int, class Turn_>;

constexpr Turn cGameEndTurn = Turn(200);
constexpr int cFieldWidth  = 10000;
constexpr int cFieldHeight = 10000;
constexpr int cFieldRight  = cFieldWidth - 1;
constexpr int cFieldLeft = 0;
constexpr int cFieldTop  = 0;
constexpr int cFieldBottom = cFieldHeight - 1;
constexpr V2i cFieldTopLeft     = {0, 0};
constexpr V2i cFieldTopRight    = {cFieldWidth-1, 0}; //フィールドの有効領域右上(9999,0)
constexpr V2i cFieldBottomLeft  = {0, cFieldHeight-1}; //フィールドの有効領域左下(0,9999)
constexpr V2i cFieldBottomRight = {cFieldWidth-1, cFieldHeight-1}; //フィールドの有効領域右下(9999,9999)
constexpr Rect cFieldRect = {cFieldTopLeft, cFieldBottomRight};
constexpr int cMonsterHavitatTop = 2500;
constexpr int cMonsterHavitatBottom = 9999;
constexpr Battery cPowerLightBattery = Battery(5);
constexpr int cDarkLightRange = 800;
constexpr int cPowerLightRange = 2000;

bool IsSameTeam(DroneId droneId0, DroneId droneId1){
    //0,2または1,3であると仮定する
    return abs(droneId0.get() - droneId1.get()) == 2;
}

void SnapToDroneZone(V2i* pPos){
    pPos->y() = max(cFieldTop   , pPos->y());
    pPos->y() = min(cFieldBottom, pPos->y());
    pPos->x() = max(cFieldLeft  , pPos->x());
    pPos->x() = min(cFieldRight , pPos->x());
}
void SnapToCreatureZone(V2i* pPos, const Rect& havitatArea){
    if(pPos->y() > cFieldBottom){ pPos->y() = cFieldBottom;}
    else if(pPos->y() > havitatArea.bottom()){ pPos->y() = havitatArea.bottom();}
    else if(pPos->y() < havitatArea.top()){ pPos->y() = havitatArea.top();}
}



struct CreatureData{
    int mMinY; //生息域最小値
    int mMaxY; //生息地最大値
    Score mPoint; //スキャン時のスコア。初スキャンの場合は2倍。
    constexpr Rect havitatRect()const{return {{0, mMinY}, {cFieldWidth - 1, mMaxY}};}
    constexpr Score scanningScore() const{return mPoint;}
    constexpr Score firstScanningScore() const{return Score(mPoint.get()*2);}
};

const CreatureData& GetCreatureData(CreatureType type){
    static CreatureData sFishData[3] = {
        {2500, 5000, Score(1)},
        {5000, 7500, Score(2)},
        {7500, 9999, Score(3)}
    };
    static CreatureData sMonsterData = {
        cMonsterHavitatTop, cMonsterHavitatBottom, Score(-1)
    };
    if(type == cCreatureType_Monster){return sMonsterData;}
    return sFishData[type.get()];
}


struct Motion{
    V2i mPosition;
    V2i mVelocity;
};

struct CreatureProfile{
    CreatureId mId;
    CreatureType mType;
    CreatureColor mColor;
    constexpr bool isFish()    const{return mType.get() >= 0;}
    constexpr bool isMonster() const{return mType == CreatureType(-1);}
};

enum class ScanType{
    None,       //ScanもDroneScanもされていない
    Scan,       //Scanしてスコア化済み
    DroneScan   //Scanしているがまだスコア化されていない
};

struct VisibleCreature{
    CreatureId mId;
    Motion mMotion;
};

struct Drone{
    DroneId mId;
    V2i mPos;
    Emergency mEmergency;
    Battery mBattery;
};

//各ドローンがスキャンしてまだセーブはしていないクリーチャー
struct DroneScan{
    DroneId mDroneId;
    CreatureId mCreatureId;
};

struct RadarBlip{
    DroneId mDroneId;
    CreatureId mCreatureId;
    string mRadar;
};

enum class RectCornerType : int {
    TL=0, TR, BR, BL
};
struct RectCorner{
    RectCornerType mType;
    string mName;
    V2i   mDir;
};
const RectCorner cTL = {RectCornerType::TL, "TL", {-1,-1}};
const RectCorner cTR = {RectCornerType::TR, "TR", { 1,-1}};
const RectCorner cBR = {RectCornerType::BR, "BR", { 1, 1}};
const RectCorner cBL = {RectCornerType::BL, "BL", {-1, 1}};
const array<RectCorner,4> cCorner4 = {
    cTL,
    cTR,
    cBR,
    cBL
};
const RectCorner& FindRectCorner(string name){
    for(const auto& corner: cCorner4){
        if(name == corner.mName){return corner;}
    }
    ASSERT(false);
    return cTL;
}
const RectCorner& GetRectCorner(RectCornerType type){
    return cCorner4[static_cast<int>(type)];
}

struct DroneScanRange{
    DroneId mDroneId;
    Circle<int> mScanRange;
};

class Radar{
public:
    Radar(const V2i& basePoint, int lightRange, const RectCorner& dir) : mBasePoint(basePoint), mLightRange(lightRange), mDir(dir){};
    
    const RectCorner& dir() const{return mDir;}
    const V2i& basePoint() const{return mBasePoint;}
    int getLightRange() const{return mLightRange;}
    
private:
    V2i mBasePoint;
    int mLightRange;
    RectCorner mDir;
};

//推定位置情報
class EstimatedMotion{
public:
    EstimatedMotion(const V2i& pos, const Rect& area):mPos(pos),mArea(area){}
    const V2i& getPosition() const{return mPos;}
    const Rect& getArea()const{return mArea;}
    
private:
    Rect mArea;    //この中に必ずいるという領域
    V2i mPos;      //推定した1点
};

class MotionResolver{
private:
    //ターン毎のMotion情報
    struct TurnInfo{
        vector<Radar> mRadars;
        optional<Motion> mMotion;
        optional<Motion> mSimulatedMotion;
        optional<Rect> mOvarlapRect;
        optional<Rect> mFoeDroneScanRect;

        optional<Rect> blipsOverlaped(const Rect& havitatRect) const{
            if(mRadars.empty()){ return nullopt;}
            Rect r = cFieldRect;
            for(const Radar& radar: mRadars){
                r = *r.overlap(locatbleRect(havitatRect, radar));
            }
            return r;
        }
        template<typename Func>
        const Radar* findRadarIf(Func&& func) const{
            return FindIf(mRadars, func);
        }
        template<typename Func>
        void forEachDirectionalPoint(Func&& func) const{
            findRadarIf([&func](const Radar& radar){
                func(radar);
                return false;
            });
        }
    private:
        //生息領域とBlipsの観測情報から、存在しうる矩形を求める
        //可視範囲は考慮していない
        Rect locatbleRect(const Rect& havitatRect, const Radar& radar) const{
            Rect r = havitatRect;
            switch (radar.dir().mType) {
                case RectCornerType::TL: return *r.overlap(Rect(radar.basePoint(), cFieldTopLeft));
                case RectCornerType::TR: return *r.overlap(Rect(radar.basePoint(), cFieldTopRight));
                case RectCornerType::BL: return *r.overlap(Rect(radar.basePoint(), cFieldBottomLeft));
                case RectCornerType::BR: return *r.overlap(Rect(radar.basePoint(), cFieldBottomRight));
            }
            ASSERT(false);
            return Rect(V2i::zero(), V2i::zero());
        }
    };
public:
    MotionResolver(CreatureType type):mType(type){
        mTurnInfoList.emplace_back();
    }
    //ターン毎のBlipsの情報を渡す(Blipsの情報がない場合は呼ばなくていい)
    void add(const V2i& dronePos, int lightRange, const RectCorner& corner){
        mTurnInfoList.back().mRadars.emplace_back(dronePos, lightRange, corner);
    }
    //ターンごとの正確なMotionを渡す(正確な位置情報がない場合は呼ばなくていい)
    void setMotion(const Motion& motion){
        mTurnInfoList.back().mMotion = motion;
    }
    void setLastSimulatedMotion(const Motion& motion){
        mTurnInfoList.back().mSimulatedMotion = motion;
    }
    void setLastFoeDroneScanRect(const Rect& rect){
        mTurnInfoList.back().mFoeDroneScanRect = rect;
    }
    
    void update(){
        const auto latest = mTurnInfoList.back();
        if(latest.mRadars.empty()){ //追い出され済み or なにかロジックエラー
            mEstimatedMotion.reset();
            mOverlapRect.reset();
            mVisibleMotion.reset();
            mSimulatedMotion.reset();
            mTurnInfoList.emplace_back();
            return;
        }
        if(latest.mMotion){
            //最新かつ正確な情報がある
            mVisibleMotion = latest.mMotion;
            auto pos = latest.mMotion->mPosition;
            mEstimatedMotion.emplace(pos, Rect{pos, pos});
            mOverlapRect = Rect{pos, pos};
            mSimulatedMotion = latest.mSimulatedMotion;
            mTurnInfoList.emplace_back();
            return;
        }
        const CreatureData& data = GetCreatureData(mType);
        
        //正確な情報はないので、仮想の位置を計算する
        //生息領域とレーダーから存在しうる範囲
        Rect rect = *latest.blipsOverlaped(data.havitatRect()); //latestにレーダー情報が含まれることは確認済みなので、nulloptは返らない
        if(latest.mFoeDroneScanRect){
            if(auto overlapred = latest.mFoeDroneScanRect->overlap(rect)){
                rect = *overlapred;
            }
        }
        
        V2i diff(100, 100); //あり得る誤差の半径を魚の速度の半分の大きさでふんわり表現
        if(latest.mSimulatedMotion && rect.contains(latest.mSimulatedMotion->mPosition)){
            Rect lastRect = {latest.mSimulatedMotion->mPosition-diff, latest.mSimulatedMotion->mPosition+diff};
            if(auto overlapped = lastRect.overlap(rect)){
                mEstimatedMotion.emplace(
                                         latest.mSimulatedMotion->mPosition
                                         ,*overlapped
                                         );
            }else{
                mEstimatedMotion.emplace(
                                         latest.mSimulatedMotion->mPosition
                                         ,rect
                                         );
            }
        }else{
            Rect overlapRect = rect; //前のターンの矩形上方を使ってより狭い矩形を求める
            if(mOverlapRect){
                if(auto overlapped = rect.overlap(Rect(mOverlapRect->topLeft()-diff, mOverlapRect->bottomRight()+diff))){
                    overlapRect = *overlapped;
                }
            }
            mOverlapRect = overlapRect;
            mEstimatedMotion.emplace(overlapRect.center(), overlapRect);
        }
        mVisibleMotion = nullopt;
        mTurnInfoList.emplace_back();
    }
    //追い出されたときはnulllpt、それ以外なら推定値を返す
    optional<V2i> estimatedPos() const{
        if(!mEstimatedMotion){return nullopt;}
        return mEstimatedMotion->getPosition();
    }
    optional<Rect> estimatedArea() const{
        if(!mEstimatedMotion){return nullopt;}
        return mEstimatedMotion->getArea();
    }
    const optional<EstimatedMotion>& getEstimatedMotion() const{
        return mEstimatedMotion;
    }
    const optional<Motion>& visibleMotion() const{
        return mVisibleMotion;
    }
private:
    CreatureType mType;
    optional<EstimatedMotion> mEstimatedMotion;
    optional<Rect> mOverlapRect;
    optional<Motion> mVisibleMotion;
    optional<Motion> mSimulatedMotion;
    
    vector<TurnInfo> mTurnInfoList; //レーダー情報、foeのレーダーは入力できないため、必然的にmyの情報しか入らない。ターン毎に対応するindexに保持。
};

class Creature{
public:
    Creature(const CreatureProfile& profile)
    :mProfile(profile),mMotionResolver(profile.mType){}
    
    void setVisibleMotion(const Motion& motion){mMotionResolver.setMotion(motion);}
    void setLastSimulatedMotion(const Motion& motion){
        mMotionResolver.setLastSimulatedMotion(motion);
    }
    
    void setScanTypeForMe(ScanType forMe)      {mScanTypeForMe = forMe;}
    void setScanTypeForFoe(ScanType forFoe)    {mScanTypeForFoe = forFoe;}

    void addMyRadarBlip(const V2i& dronePos, int lightRange, const RectCorner& corner){
        mMotionResolver.add(dronePos, lightRange, corner);
    }
    void addFoeDrawScanRect(const Rect& rect){
        mMotionResolver.setLastFoeDroneScanRect(rect);
    }
    void update(){
        mMotionResolver.update();
    }
     
    CreatureId id()           const{return mProfile.mId;}
    CreatureType type()       const{return mProfile.mType;}
    bool isFish()             const{return mProfile.isFish();}
    bool isMonster()          const{return mProfile.isMonster();}
    CreatureColor color()     const{return mProfile.mColor;}
    ScanType scanTypeForMe()  const{return mScanTypeForMe;}
    bool isScanTypeNoneForMe() const{return scanTypeForMe() == ScanType::None;}
    ScanType scanTypeForFoe() const{return mScanTypeForFoe;}
    bool isLostFish()         const{return !mMotionResolver.estimatedPos();}

    const optional<Motion>&  visibleMotion() const{return mMotionResolver.visibleMotion();}

    optional<V2i> estimatedPos() const{
        return mMotionResolver.estimatedPos();
    }
    optional<Rect> estimatedArea() const{
        return mMotionResolver.estimatedArea();
    }
    const optional<EstimatedMotion>& estimatedMotion()const{
        return mMotionResolver.getEstimatedMotion();
    }
    
    //正確な位置を把握しているときはそれを、追い出されているときはnulloptを、それ以外は推定値を返す
    optional<V2i> bestEffortPos() const{
        if(auto vm = visibleMotion()){return vm->mPosition;}
        return estimatedPos();
    }

    
private:
    CreatureProfile mProfile;
    ScanType mScanTypeForMe = ScanType::None;
    ScanType mScanTypeForFoe = ScanType::None;
    MotionResolver mMotionResolver;
};


class Player{
public:
    Player():mScore(0){
    }
    void setScore(Score score){mScore = score;}
    Score getScore()const{return mScore;}
    void setScans(const vector<CreatureId>& scans){mScans = scans;}
    void updateDrone(DroneId droneId, const V2i& pos, Emergency emergency, Battery battery){
        if(Drone* pDrone = findDrone_(droneId)){
            pDrone->mId = droneId; pDrone->mPos = pos; pDrone->mEmergency = emergency; pDrone->mBattery = battery;
            return;
        }
        mDrones.push_back(Drone{droneId,pos,emergency,battery});
    }

    const Drone* findDrone(DroneId id) const { return findDroneIf([id](const Drone& drone){return drone.mId == id;});}

    template<typename Func>
    const Drone* findDroneIf(Func&& func)const{
        for(const Drone& drone: mDrones){if(func(drone)){return &drone;}}
        return nullptr;
    }

    template<typename Func>
    void forEachDrone(Func&& func)const{
        findDroneIf([&func](const Drone& d){
            func(d);
            return false;
        });
    }
    const Drone& getFirstDrone()const{return getDrone(0);}
    const Drone& getDrone(int idx)const{return mDrones[idx];}

    const vector<CreatureId>& getScans()const{return mScans;}
    const vector<Drone>& getDrones()const{return mDrones;}

    bool containsScaned(CreatureId id) const{return find(mScans.begin(), mScans.end(), id) != mScans.end();}
    
private:
    Drone* findDrone_(DroneId id) {return const_cast<Drone*>(as_const(*this).findDrone(id));}

    Score mScore;
    vector<CreatureId> mScans; //スコア化済みのクリーチャー
    vector<Drone> mDrones;
};

class GameScorer{
private:
    using PlayerIdx = NamedType<int, class PlayerIdx_>;
    static constexpr PlayerIdx cMyIdx = PlayerIdx(0);
    static constexpr PlayerIdx cFoeIdx = PlayerIdx(1);

public:
    GameScorer(const vector<CreatureProfile>& profiles){
        for(const CreatureProfile& cp: profiles){
            if(!cp.isFish()){continue;}
            mScanTurns.emplace_back(cp);
        }
    }
    void scanByMe(CreatureId id, Turn turn){
        ScanTurn* pScanTurn = find_(id);
        if(pScanTurn->getTurn(cMyIdx)){return;}
        pScanTurn->setTurn(cMyIdx, turn);
    }
    void scanByFoe(CreatureId id, Turn turn){
        ScanTurn* pScanTurn = find_(id);
        if(pScanTurn->getTurn(cFoeIdx)){return;}
        pScanTurn->setTurn(cFoeIdx, turn);
    }
    Score myScore() const{
        return score_(cMyIdx, cFoeIdx);
    }
    Score foeScore() const{
        return score_(cFoeIdx, cMyIdx);
    }
    optional<Turn> findMyScanTurn(CreatureId id)const{
        return findScanTurn_(cMyIdx, id);
    }
    optional<Turn> findFoeScanTurn(CreatureId id)const{
        return findScanTurn_(cFoeIdx, id);
    }
    //スキャンしたターンを格納する
    class ScanTurn{
    public:
        ScanTurn(const CreatureProfile& profile):mProfile(profile){}
        CreatureId id()const{return mProfile.mId;}
        CreatureColor color()const{return mProfile.mColor;}
        CreatureType type()const{return mProfile.mType;}
        //スキャンによるスコアを返す。標準スコア、その倍、、0のいずれか。
        Score scanningScore(PlayerIdx targetIdx, PlayerIdx otherIdx) const{
            const CreatureData& data = GetCreatureData(mProfile.mType);
            return result(targetIdx, otherIdx, data.firstScanningScore(), data.scanningScore(), Score(0));
        }
        void setTurn(PlayerIdx idx, Turn turn){
            auto& curTurn = getTurn_(idx);
            ASSERT(!curTurn|| (*curTurn== turn));
            curTurn = turn;
        }
        
        //自分と相手のスキャンしたTurnを比較し、3つの結果のいずれかを返す
        //結果に応じて返してほしい値を引数で渡す
        //結果は「自分が相手と同時または先にスキャンしている」「自分もスキャンしているが、相手の方がより早い」「自分はスキャンしていない」の３つ
        template<typename T>
        T result(PlayerIdx targetIdx, PlayerIdx otherIdx, T earlier, T slower, T none)const{
            ASSERT(targetIdx != otherIdx);
            const auto& targetTurn = getTurn(targetIdx);
            if(!targetTurn){return none;}
            const auto& otherTurn = getTurn(otherIdx);
            return (!otherTurn || (*targetTurn <= *otherTurn)) ? earlier : slower;
        }
        const optional<Turn>& getTurn(PlayerIdx idx)const{return mScanTurns[idx.get()];}
    private:
        optional<Turn>& getTurn_(PlayerIdx idx){return mScanTurns[idx.get()];}
        
        CreatureProfile mProfile;
        array<optional<Turn>,2> mScanTurns;
    };
    //ボーナス算出用
    class BonusTurn{
    public:
        bool evalTurn(const optional<Turn>& turn){
            if(!turn){return false;} //ボーナス成功していないことが確定
            if(!mWorstTurn || *turn > *mWorstTurn){
                mWorstTurn = turn;
            }
            return true; //達成見込みはまだある
        }
        //ボーナスを達成したターン
        const optional<Turn>& getTurn() const{return mWorstTurn;}
    private:
        optional<Turn> mWorstTurn;
    };
    
    
private:
    ScanTurn* find_(CreatureId id){return const_cast<ScanTurn*>(as_const(*this).find_(id));}
    const ScanTurn* find_(CreatureId id) const {return FindIf(mScanTurns, [id](const ScanTurn& st){return st.id() == id;});}
    
    optional<Turn> findScanTurn_(PlayerIdx idx, CreatureId id)const{
        const ScanTurn* pScanTurn = find_(id);
        ASSERT(pScanTurn);
        return pScanTurn->getTurn(idx);
    }

    Score allColorBonusScore_(PlayerIdx targetIdx, PlayerIdx otherIdx)const{
        Score s = Score(0);
        for(int i=0;i<=3;i++){
            s += colorBonusScore_(CreatureColor(i), targetIdx, otherIdx);
        }
        return s;
    }
    Score colorBonusScore_(CreatureColor color, PlayerIdx targetIdx, PlayerIdx otherIdx)const{
        optional<Turn> targetBonusTurn = colorBonusTurn_(targetIdx, color);
        if(!targetBonusTurn){
            return Score(0); //ボーナス達成せず
        }
        optional<Turn> otherBonusTurn = colorBonusTurn_(otherIdx, color);
        if(!otherBonusTurn || *targetBonusTurn <= *otherBonusTurn){
            return Score(6); //相手より早く達成
        }
        return Score(3); //相手より遅く達成
    }
    optional<Turn> colorBonusTurn_(PlayerIdx idx, CreatureColor color)const{
        BonusTurn bonusTurn;
        int count = 0;
        for(const ScanTurn& sc: mScanTurns){
            if(sc.color() != color){continue;}
            if(!bonusTurn.evalTurn(sc.getTurn(idx))){
                return nullopt;
            }
            count++;
        }
        ASSERT(count==3); //同色は3匹
        return bonusTurn.getTurn();
    }
    Score allTypeBonusScore_(PlayerIdx targetIdx, PlayerIdx otherIdx)const{
        Score s = Score(0);
        for(int i=0;i<=2;i++){
            s += typeBonusScore_(CreatureType(i), targetIdx, otherIdx);
        }
        return s;
    }
    Score typeBonusScore_(CreatureType type, PlayerIdx targetIdx, PlayerIdx otherIdx)const{
        optional<Turn> targetBonusTurn = typeBonusTurn_(targetIdx, type);
        if(!targetBonusTurn){
            return Score(0); //ボーナス達成せず
        }
        optional<Turn> otherBonusTurn = typeBonusTurn_(otherIdx, type);
        if(!otherBonusTurn || *targetBonusTurn <= *otherBonusTurn){
            return Score(8); //相手より早く達成
        }
        return Score(4); //相手より遅く達成
    }
    optional<Turn> typeBonusTurn_(PlayerIdx idx, CreatureType type)const{
        BonusTurn bonusTurn;
        int count = 0;
        for(const ScanTurn& sc: mScanTurns){
            if(sc.type() != type){continue;}
            if(!bonusTurn.evalTurn(sc.getTurn(idx))){
                return nullopt;
            }
            count++;
        }
        ASSERT(count==4); //同typeは4匹
        return bonusTurn.getTurn();
    }
    Score score_(PlayerIdx targetIdx, PlayerIdx otherIdx) const{
        Score s = Score(0);
        for(const auto& scanTurn: mScanTurns){
            s += scanTurn.scanningScore(targetIdx, otherIdx);
        }
        s += allColorBonusScore_(targetIdx, otherIdx);
        s += allTypeBonusScore_(targetIdx, otherIdx);
        return s;
    }

    vector<ScanTurn> mScanTurns;
};

class PlayField{
public:
    PlayField(const vector<CreatureProfile>& profiles)
    : mTurn(0)
    , mCreatureProfiles(profiles)
    , mScorer(profiles)
    {}
    
    void setTurn(Turn turn){mTurn = turn;}
    Turn getTurn() const{return mTurn;}
    
    Player* getMyPlayer(){return &mMyPlayer;}
    Player* getFoePlayer(){return &mFoePlayer;}
    void setDroneScans(const vector<DroneScan>& droneScans){mDroneScans = droneScans;}
    void setVisibleCreatures(const vector<VisibleCreature>& visibleCreatures){mVisibleCreatures = visibleCreatures;}
    void setRadarBlips(const vector<RadarBlip>& radarBlips){mMyRadarBlips = radarBlips;}
    void setLastPowerLightDrones(const vector<DroneId>& droneIds){
        mLastPowerLightDroneIds = droneIds;
    }
    void setSimulatedMotions(const vector<pair<CreatureId, Motion>>& motions){
        mSimulatedMotions = motions;
    }
    //各set関数で設定した情報をもとに必要なら統合しつつPlayFieldの情報を更新する
    //Creatureの情報と、GameScorerが更新対象
    void update(){
        for(const CreatureProfile& profile: mCreatureProfiles){
            Creature* pCreature = findCreature_(profile.mId);
            if(!pCreature){
                pCreature = &mCreatures.emplace_back(profile);
            }
            if(const VisibleCreature* pVisibleCreature = FindIf(mVisibleCreatures, [id=profile.mId](const VisibleCreature& vc){
                return vc.mId == id;
            })){
                pCreature->setVisibleMotion(pVisibleCreature->mMotion);
            }
            if(const auto* pPair = FindIf(mSimulatedMotions, [id=profile.mId](const auto& pr){
                return pr.first == id;
            })){
                pCreature->setLastSimulatedMotion(pPair->second);
            }
            pCreature->setScanTypeForMe(ScanType::None);
            pCreature->setScanTypeForFoe(ScanType::None);
            for(const DroneScan& ds: mDroneScans){
                if(ds.mCreatureId == pCreature->id()){
                    if(mMyPlayer.findDrone(ds.mDroneId)){
                        pCreature->setScanTypeForMe(ScanType::DroneScan);
                    }
                    if(mFoePlayer.findDrone(ds.mDroneId)){
                        pCreature->setScanTypeForFoe(ScanType::DroneScan);
                    }
                }
            }
            if(mMyPlayer.containsScaned(pCreature->id())){
                pCreature->setScanTypeForMe(ScanType::Scan);
                mScorer.scanByMe(pCreature->id(), mTurn - Turn(1));
            }
            if(mFoePlayer.containsScaned(pCreature->id())){
                pCreature->setScanTypeForFoe(ScanType::Scan);
                mScorer.scanByFoe(pCreature->id(), mTurn - Turn(1));
            }
            for(const RadarBlip& rb: mMyRadarBlips){
                if(rb.mCreatureId == pCreature->id()){
                    pCreature->addMyRadarBlip(
                                              findMyDrone(rb.mDroneId)->mPos
                                              ,containsLastPowerLightDroneIds(rb.mDroneId) ? cPowerLightRange : cDarkLightRange
                                              ,FindRectCorner(rb.mRadar)
                                              );
                }
            }
            pCreature->update();
            LOG("Creature " << pCreature->id().get()
                << ", vis:" << (pCreature->visibleMotion()?pCreature->visibleMotion()->mPosition.toString():"un")
                << ", est:" << (pCreature->estimatedPos()?pCreature->estimatedPos()->toString():"un")
                << ", rct:" << (pCreature->estimatedArea()?pCreature->estimatedArea()->toString():"un") << endl);
        }
    }
    
    const Drone& getMyDrone(int idx)const{return mMyPlayer.getDrone(idx);}

    const Drone* findMyDrone(DroneId id)  const{ return mMyPlayer.findDrone(id);}
    const Drone* findFoeDrone(DroneId id) const{ return mFoePlayer.findDrone(id);}

    template<typename Func>
    const Creature* findCreatureIf(Func&& func) const{
        for(const Creature& c: mCreatures){if(func(c)){return &c;}}
        return nullptr;
    }

    template<typename Func>
    const Creature* findMonsterIf(Func&& func) const{
        return findCreatureIf([&func](const Creature& c){
            return c.isMonster() && func(c);
        });
    }

    const Creature* findCreature(CreatureId id) const{
        return findCreatureIf([id](const Creature& c){return c.id() == id;});
    }

    template<typename Func>
    void forEachCreature(Func&& func) const{
        findCreatureIf([&func](const Creature& c){
            func(c);
            return false;
        });
    }
    template<typename Func>
    void forEachFish(Func&& func) const{
        forEachCreature([&func](const Creature& c){
            if(c.isFish()){
                func(c);
            }
        });
    }
    template<typename Func>
    void forEachActiveFish(Func&& func) const{
        forEachFish([&func](const Creature& c){
            if(!c.isLostFish()){
                func(c);
            }
        });
    }


    template<typename Func>
    void forEachMonster(Func&& func) const{
        findMonsterIf([&func](const Creature& c){
            func(c);
            return false;
        });
    }

    template<typename Func>
    const DroneScan* findDroneScanIf(Func&& func) const{
        for(const DroneScan& ds: mDroneScans){ if(func(ds)){return &ds;} }
        return nullptr;
    }
    //droneIdが指すDroneが保持するDroneScanをfindIfする
    template<typename Func>
    const DroneScan* findDroneScanIf(DroneId droneId, Func&& func) const {
        return findDroneScanIf([droneId, &func](const DroneScan& ds){return ds.mDroneId == droneId && func(ds);});
    }
    //droneIdが指すDroneが保持する最初のDroneScanを返す。複数ある場合、最初の1つになる。
    const DroneScan* findDroneScan(DroneId droneId) const{ return findDroneScanIf([droneId](const DroneScan& ds){return ds.mDroneId == droneId;}); }
    //droneIdが指すDroneが保持するcreatureIdを対象としたDroneScaneを返す。
    const DroneScan* findDroneScan(DroneId droneId, CreatureId creatureId) const{ return findDroneScanIf([droneId, creatureId](const DroneScan& ds){return ds.mDroneId == droneId && ds.mCreatureId == creatureId;}); }

    template<typename Func>
    void forEachDroneScan(DroneId droneId, Func&& func) const{
        findDroneScanIf(droneId, [&func](const DroneScan& ds){
            func(ds);
            return false;
        });
    }
    template<typename Func>
    void forEachMyDroneScan(Func&& func) const{
        for(const DroneScan& ds: mDroneScans){
            if(findMyDrone(ds.mDroneId)){
                func(ds);
            }
        }
    }
    template<typename Func>
    void forEachFoeDroneScan(Func&& func) const{
        for(const DroneScan& ds: mDroneScans){
            if(findFoeDrone(ds.mDroneId)){
                func(ds);
            }
        }
    }

    
    const vector<CreatureProfile>& getCreatureProfiles()const{return mCreatureProfiles;}
    Score getMyScore()const{return mMyPlayer.getScore();}
    Score getFoeScore()const{return mFoePlayer.getScore();}
    const vector<CreatureId>& getMyScans() const{return mMyPlayer.getScans();}
    const vector<CreatureId>& getFoeScans() const{return mFoePlayer.getScans();}
    const vector<Drone>& getMyDrones() const{return mMyPlayer.getDrones();}
    const vector<Drone>& getFoeDrones() const{return mFoePlayer.getDrones();}
    const vector<DroneScan>& getDroneScans() const{return mDroneScans;}
    const vector<VisibleCreature>& getVisibleCreatures() const{return mVisibleCreatures;}
    const GameScorer& getScorer() const{return mScorer;}
    
private:
    bool containsLastPowerLightDroneIds(DroneId id) const{
        return FindIf(mLastPowerLightDroneIds, [&id](DroneId did){return did==id;});
    }
    
    Creature* findCreature_(CreatureId id){
        return const_cast<Creature*>(findCreature(id));
    }

    Turn mTurn;
    Player mMyPlayer;
    Player mFoePlayer;
    GameScorer mScorer;
    vector<DroneScan> mDroneScans; //スキャン済みだが、まだセーブできていないもの。敵の情報も味方の情報も両方入る。
    vector<RadarBlip> mMyRadarBlips; //自分のドローンに対して、クリーチャーのいちを大まかに知ることができる
    vector<Creature> mCreatures;
    array<Creature*, 20> mCreatureCaches;
    vector<CreatureProfile> mCreatureProfiles;
    vector<VisibleCreature> mVisibleCreatures;
    vector<DroneId> mLastPowerLightDroneIds;
    vector<pair<CreatureId, Motion>> mSimulatedMotions;
};

class PlayFieldUpdater{
public:
    PlayFieldUpdater(istream& is):mInput(is){
        int count = mInput.nextInt(); mInput.ignore();
        vector<CreatureProfile> profiles;
        for(int i=0;i<count;i++){
            int id, color, type;
            mInput >> id >> color >> type; mInput.ignore();
            profiles.emplace_back(CreatureProfile{CreatureId(id), CreatureType(type), CreatureColor(color)});
        }
        mPlayField.emplace(profiles);
    }
    
    //1ターン分の入力からビルド
    const PlayField& update(){
        mPlayField->setTurn(mTurn);
        Player* pMyPlayer = mPlayField->getMyPlayer();
        Player* pFoePlayer = mPlayField->getFoePlayer();
        int myScore = mInput.nextInt(); mInput.ignore();
        int foeScore = mInput.nextInt(); mInput.ignore();
        pMyPlayer->setScore(Score(myScore));
        pFoePlayer->setScore(Score(foeScore));
        pMyPlayer->setScans(buildScans());
        pFoePlayer->setScans(buildScans());
        for(const Drone& d: buildDrones()){ pMyPlayer->updateDrone(d.mId, d.mPos, d.mEmergency, d.mBattery); }
        for(const Drone& d: buildDrones()){ pFoePlayer->updateDrone(d.mId, d.mPos, d.mEmergency, d.mBattery); }
        mPlayField->setDroneScans(buildDroneScans());
        mPlayField->setVisibleCreatures(buildVisibleCreatures());
        mPlayField->setRadarBlips(buildRadarBlips());
        mPlayField->setLastPowerLightDrones(mPowerLightDroneIds);
        mPlayField->setSimulatedMotions(mSimulatedMotions);
        mPlayField->update();
        LOGLN("\nDone\n");
        mTurn += Turn(1);
        mPowerLightDroneIds.clear();
        mSimulatedMotions.clear();
        return *mPlayField;
    }
    
    void recordPowerLight(DroneId id){
        mPowerLightDroneIds.push_back(id);
    }
    void addSimulatedPos(CreatureId id, const Motion& motion){
        mSimulatedMotions.push_back({id, motion});
    }
    
private:
    vector<CreatureId> buildScans(){
        vector<CreatureId> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id;
            mInput >> id; mInput.ignore();
            ret.emplace_back(CreatureId(id));
        }
        return ret;
    }
    vector<Drone> buildDrones(){
        vector<Drone> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id, x, y, emergency, battery;
            mInput >> id >> x >> y >> emergency >> battery; mInput.ignore();
            ret.emplace_back(Drone{DroneId(id), V2i(x,y), Emergency(emergency), Battery(battery)});
        }
        return ret;
    }
    vector<DroneScan> buildDroneScans(){
        vector<DroneScan> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int droneId, creatureId;
            mInput >> droneId >> creatureId; mInput.ignore();
            ret.emplace_back(DroneScan{DroneId(droneId), CreatureId(creatureId)});
        }
        return ret;
    }
    vector<VisibleCreature> buildVisibleCreatures(){
        vector<VisibleCreature> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int id, x, y, vx, vy;
            mInput >> id >> x >> y >> vx >> vy; mInput.ignore();
            ret.emplace_back(VisibleCreature{CreatureId(id), Motion{V2i(x, y), V2i(vx, vy)}});
        }
        return ret;
    }
    vector<RadarBlip> buildRadarBlips(){
        vector<RadarBlip> ret;
        int count = mInput.nextInt(); mInput.ignore();
        for(int i=0;i<count;i++){
            int droneId, creatureId;
            string radar;
            mInput >> droneId >> creatureId >> radar; mInput.ignore();
            ret.emplace_back(RadarBlip{DroneId(droneId), CreatureId(creatureId), radar});
        }
        return ret;
    }

    optional<PlayField> mPlayField;
    vector<DroneId> mPowerLightDroneIds;
    vector<DroneScan> mLastDroneScan;
    vector<pair<CreatureId, Motion>> mSimulatedMotions;
    Input mInput;
    Turn mTurn = Turn(0);
};

class Command{
public:
    void setMove(const V2i& to){
        mMoveTo=to;
    }
    void enablePowerLight(){mIsEnablePowerLight=true;}
    
    const optional<V2i> getMoveTo() const{return mMoveTo;}
    bool isEnabledPowerLight()const{return mIsEnablePowerLight;}
    
    string toString() const{
        stringstream ss;
        if(mMoveTo){ ss << "MOVE " << mMoveTo->x() << " " << mMoveTo->y();}
        else {ss << "WAIT";}
        ss << " " << (mIsEnablePowerLight ? "1" : "0");
        return ss.str();
    }
    string toShortString() const{
        stringstream ss;
        if(mMoveTo){ ss << "M " << mMoveTo->x() << " " << mMoveTo->y();}
        else {ss << "W";}
        ss << " " << (mIsEnablePowerLight ? "L" : "D");
        return ss.str();
    }
private:
    optional<V2i> mMoveTo; //有効ならMOVE、そうでなければWAIT
    bool mIsEnablePowerLight = false;
};

struct DronesCommand{
    DroneId mDroneId{-1};
    Command mCommand;
    
    void out(ostream& os) const{
        os << mCommand.toString() << endl;
    }
    bool isEnabledPowerLight() const{
        return mCommand.isEnabledPowerLight();
    }
};

bool DroneAndMonsterCollide(const V2i& dronePos, const V2i& droneSpeed, const V2i& monsterPos, const V2i& monsterSpeed){
    constexpr int cDroneHitRange = 200;
    constexpr int cMonsterEatRange = 300;
    constexpr int cHitRange = cDroneHitRange + cMonsterEatRange;
    //始点の時点で衝突していて避けようがない
    if(dronePos.isInRange(monsterPos, cHitRange)){
        return true;
    }
    if(droneSpeed.isZero()&&monsterPos.isZero()){return false;}
    double x = monsterPos.x() - dronePos.x();
    double y = monsterPos.y() - dronePos.y();
    double r = cHitRange;
    double vx = monsterSpeed.x() - droneSpeed.x();
    double vy = monsterSpeed.y() - droneSpeed.y();
    double a = vx * vx + vy * vy;
    if(a <= 0.0){return false;} //相対速度が0、位置関係は変わらないので衝突しない
    double b = 2.0 * (x * vx + y * vy);
    double c = x * x + y * y - r * r;
    double delta = b * b - 4.0 * a * c;
    if(delta < 0.0){return false;}
    double t = (-b - sqrt(delta)) / (2.0 * a);
    if(t <= 0.0 || t > 1.0){return false;}
    return true;
}

class CreatureMotion{
public:
    CreatureMotion(const Motion& motion):mMotion(motion){}
    CreatureMotion(const EstimatedMotion& motion)
    : mMotion({motion.getPosition(), V2i::zero()})
    , mEstimatedArea(motion.getArea())
    {}
    
    //現在位置の取得、推定したものかそうでないかは区別しない
    const V2i& getPos() const{return mMotion.mPosition;}
    int x() const{return getPos().x();}
    int y() const{return getPos().y();}
    
    const V2i& getVelocity() const{return mMotion.mVelocity;}
    void setVelocity(const V2i& vel){mMotion.mVelocity = vel;}
    
    //現在位置が推定されたものの場合、生息可能性がある領域を取得できる
    //推定ではない場合はこれはnullopt
    const optional<Rect>& getEstimatedArea() const{
        return mEstimatedArea;
    }
    
    const Motion& getMotion() const{
        return mMotion;
    }

    //移動処理(現在の位置座標にvelocityを加算し、snapする)
    void moveAndSnap(const Rect& rect){
        mMotion.mPosition += mMotion.mVelocity;
        SnapToCreatureZone(&mMotion.mPosition, rect);
    }
    
    V2i nextPos() const{
        return mMotion.mPosition + mMotion.mVelocity;
    }
private:
    Motion mMotion;
    optional<Rect> mEstimatedArea;
};

class SimulatorCreature{
public:
    SimulatorCreature(const CreatureProfile& profile):mProfile(profile){
    };
    CreatureId id() const{return mProfile.mId;}
    void setCreatureMotion(const CreatureMotion& motion){ mMotion.emplace(motion); }
    const optional<CreatureMotion>& getCreatureMotion() const{return mMotion;}
    optional<Motion> getMotion() const{
        return mMotion ? optional<Motion>{mMotion->getMotion()} : nullopt;
    }
    bool isMonster() const{return mProfile.isMonster();}
    bool isFish() const {return mProfile.isFish();}
    bool isLostFish() const {return mIsLostFish;}
    bool willBeLostFish() const{
        return isFish() && mMotion
        && isOutOfField_(mMotion->nextPos());
    }
    optional<int> fishLostEstimatedTurn() const{
        return mFishLostEstimatedTurn;
    }
    void updatePos(){
        if(mIsLostFish){mMotion = nullopt; return;}
        if(!mMotion){return;}
        mMotion->moveAndSnap(GetCreatureData(mProfile.mType).havitatRect());
        if(isOutOfField_()){
            //場外に出た魚を退場(mCreaturesから削除)
            ASSERT(isFish());
            mIsLostFish = true;
        }
    }
    void updateMonsterTarget(const vector<DroneScanRange>& droneScanRanges){
        mMonsterTarget.reset();
        if(!isMonster()){return;}
        if(!mMotion){return;}
        Closest closest(mMotion->getPos());
        for(const DroneScanRange& dsr: droneScanRanges){
            if(!dsr.mScanRange.collides(mMotion->getPos())){continue;}
            closest.eval(dsr.mScanRange.center());
        }
        if(!closest.empty()){
            mMonsterTarget = closest.meanPos();
        }
    }
    void updateAsFish(const vector<V2i>& engineOnDronesPosList, const vector<V2i>& otherFishesPosList){
        if(!isFish()){return;}
        if(!mMotion){return;}
        mFishLostEstimatedTurn.reset();
        Closest closestDrones(mMotion->getPos());
        for(const V2i& pos: engineOnDronesPosList){
            closestDrones.eval(pos);
        }
        //「一番近いエンジン起動中ドローンから逃げる」の発動
        //この場合のみフィールドの横方向から逃げ出す可能性がある
        if(!closestDrones.empty() && closestDrones.sqLength() <= 1400 * 1400){
            auto relative = mMotion->getPos() - closestDrones.meanPos();
            mMotion->setVelocity(relative.isZero() ? V2i::zero() : relative.normalize(400));
            int vx = mMotion->getVelocity().x();
            if(vx == 0){
                mFishLostEstimatedTurn = 10000;
            } else {
                int distance = vx > 0 ? cFieldWidth - mMotion->x() : mMotion->x();
                mFishLostEstimatedTurn = distance/abs(vx);
            }
            return;
        }
        //今の進行方向に200の速さで移動するベクトルを求める
        V2d v = mMotion->getVelocity();
        if(!v.isZero()){
            v.normalize(200.0);
        }
        //「最も近いFishから逃げる」が発動する場合はvを変更する
        {
            Closest closestFishes(mMotion->getPos());
            for(const V2i& pos: otherFishesPosList){
                closestFishes.eval(pos);
            };
            if(!closestFishes.empty() && closestFishes.sqLength() <= 600 * 600){
                V2d avoid = closestFishes.meanPos();
                auto relative = V2d(mMotion->getPos()) - avoid;
                v = relative.isZero() ? V2d::zero() : relative.normalize(200);
            }
        }
        V2d nextPos = mMotion->getPos() + v;
        if((nextPos.x() < 0 && nextPos.x() < mMotion->x())
            ||(nextPos.x() > cFieldRight && nextPos.x() > mMotion->x()))
        {
            v.x() = -v.x();
        }
        const CreatureData& cd = GetCreatureData(mProfile.mType);
        if((nextPos.y() < cd.mMinY && nextPos.y() < mMotion->y())
            ||(nextPos.y() > cd.mMaxY && nextPos.y() > mMotion->y()))
        {
            v.y() = -v.y();
        }
        //TODO: epsilonRoundの実装...?
        mMotion->setVelocity(v.round());
    }
    
    void updateAsMonster(const vector<SimulatorCreature>& creatures){
        if(!isMonster()){return;}
        if(!mMotion){return;}
        if(mMonsterTarget){
            //攻撃するぜ
            V2d attackVec = mMotion->getPos() - *mMonsterTarget;
            if(attackVec.sqLength() > 540 * 540){
                attackVec = attackVec.normalize(540);
            }
            mMotion->setVelocity(attackVec);
            return;
        }
        V2d v = mMotion->getVelocity();
        
        if(v.sqLength() > 270 * 270){
            v = v.normalize(270).round();
        }
        if(!v.isZero()){
            Closest closest(mMotion->getPos());
            for(const SimulatorCreature& sc: creatures){
                if(!sc.isMonster() || !sc.getMotion()){continue;}
                closest.eval(sc.getMotion()->mPosition);
            }
            if(!closest.empty() && closest.sqLength() <= 600 * 600){
                V2d avoidDir = (closest.meanPos() - mMotion->getPos());
                if(!avoidDir.isZero()){
                    v = avoidDir.normalize(200).round();
                }
            }
        }
        V2d nextPos = mMotion->nextPos();
        if((nextPos.x() < 0 && nextPos.x() < mMotion->x())
           || (nextPos.x() > cFieldRight && nextPos.x() > mMotion->x()) ){
            v.x() *= -1;
        }
        if((nextPos.y() < cMonsterHavitatTop && nextPos.y() < mMotion->y())
           || (nextPos.y() > cMonsterHavitatBottom && nextPos.y() > mMotion->y()) ){
            v.y() *= -1;
        }
        mMotion->setVelocity(v);
    }
    string toString()const{
        stringstream ss;
        ss << "[" << mProfile.mId << "]";
        if(mMotion){
            ss << "pos:"<<mMotion->getPos().toString()<<",vel:"<<mMotion->getVelocity().toString();
        }else{
            ss << "unknown";
        }
        ss << (isLostFish() ? "[lost]":"") << (willBeLostFish()?"[next lost]":"");
        if(mFishLostEstimatedTurn){
            ss <<"[lost after "<<*mFishLostEstimatedTurn<<" turn]";
        }
        return ss.str();
    }
    
private:
    bool isOutOfField_(const V2i& pos)const{return pos.x() < cFieldLeft || pos.x() > cFieldRight;}
    bool isOutOfField_()const{return mMotion && isOutOfField_(mMotion->getPos()); }


    CreatureProfile mProfile;
    optional<CreatureMotion> mMotion;   //場外に出ていなければ有効な値
    optional<V2d> mMonsterTarget;       //モンスターのみ有効な値が入りうる
    bool mIsLostFish = false;           //場外に出たため消失した扱い
    optional<double> mFishLostEstimatedTurn; //今逃げている最中なら、このままの速度で逃げ続けたとして何ターン後に場外に行くか
};

class SimulatorDrone{
public:
    SimulatorDrone(const Drone& drone, bool isMe):mDrone(drone),mIsMe(isMe){}
    DroneId id() const{return mDrone.mId;}
    bool isMe()const{return mIsMe;}
    bool isFoe()const{return !mIsMe;}
    bool isEngineOn()const{return mIsEngineOn;}
    Battery getBattery() const{return mDrone.mBattery;}
    bool canScan(const V2i& pos) const{return !isDeadOrDying() && mDrone.mPos.isInRange(pos, lightRange());}
    double scanWeight(const CreatureMotion& motion)const{
        if(isDeadOrDying()){return 0.0;}
        if(const auto& estimatedArea = motion.getEstimatedArea()){
            Rect approximateScanCircle = {mDrone.mPos - V2i{lightRange()/2, lightRange()/2}, mDrone.mPos + V2i{lightRange()/2, lightRange()/2}};
#if 0
            int count = 0;
            for(V2i p: {estimatedArea->topLeft(), estimatedArea->topRight(), estimatedArea->bottomLeft(), estimatedArea->bottomRight()}){
                if(canScan(p)){
                    count++;
                }
            }
            return count >= 2;
#endif
            if(optional<Rect> scanningRect = approximateScanCircle.overlap(*motion.getEstimatedArea())){
                return min(1.0, scanningRect->area()/(double)motion.getEstimatedArea()->area());
            }else{
                return 0.0; //全く重なっていないならスキャンできている可能性はなし
            }
        }else{
            return canScan(motion.getPos()) ? 1.0 : 0.0;
        }

    }
    bool isBatteryFull()const{return mDrone.mBattery >= Battery(30);}
    const V2i& getPos() const{return mDrone.mPos;}
    
    void updatePowerLight(bool powerLight){
        mIsEnabledPowerLight = powerLight && mDrone.mBattery.get() >= 5;
    };
    void updateBattery(){
        if(mIsEnabledPowerLight){
            ASSERT(mDrone.mBattery >= cPowerLightBattery);
            mDrone.mBattery -= cPowerLightBattery;
        } else {
            mDrone.mBattery += Battery(1);
            if(mDrone.mBattery > Battery(30)){
                mDrone.mBattery = Battery(30);
            }
        }
    }
    void updateDroneSpeed(const optional<V2i>& moveTo){
        constexpr int cSpeed = 600;
        if(mIsDead){
            mSpeed = {0, -400};
            mIsEngineOn = false;
        } else if(moveTo){
            double x = moveTo->x() - mDrone.mPos.x();
            double y = moveTo->y() - mDrone.mPos.y();
            double len = sqrt(x*x+y*y);
            if(len > cSpeed){
                x/=len; y/=len;
                x*=cSpeed; y*=cSpeed;
            }
            mSpeed = V2i(int(round(x)), int(round(y)));
            mIsEngineOn = true;
        } else if(mDrone.mPos.y() < cFieldHeight - 1){
            mSpeed = V2i(0, 300);
            mIsEngineOn = false;
        } else {
            mSpeed = V2i(0, 0);
            mIsEngineOn = false;
        }
    }
    bool updateDying(const vector<SimulatorCreature>& creatures){
        if(mIsDead){return true;}
        for(const SimulatorCreature& sc: creatures){
            if(!sc.isMonster()){continue;}
            const auto& motion = sc.getMotion();
            if(!motion){continue;} //未知の場合にどう取り扱うか要検討
            if(DroneAndMonsterCollide(mDrone.mPos, mSpeed, motion->mPosition, motion->mVelocity)){
                mIsDying = true;
                return true;
            }
        }
        return false;
    }
    void updateDronePos(){
        mDrone.mPos += mSpeed;
        SnapToDroneZone(&mDrone.mPos);
    }
    void updateDead(){
        if(mIsDying){
            mIsDying = false;
            mIsDead = true;
        }else if(mDrone.mPos.y() == 0){
            mIsDead = false;
        }
    }
    //! スキャン可能な円領域を返す。Droneが死亡中ならnulloptを返す
    optional<DroneScanRange> droneScanRange() const{
        if(isDeadOrDying()){return nullopt;}
        return {{id(), {getPos(), lightRange()}}};
    }
        
    
    bool isDeadOrDying() const {return mIsDead || mIsDying;}
    bool isInScoringArea() const {return mDrone.mPos.y() <= 500;}
    
    string toString() const{
        stringstream ss;
        ss << "id:"<<mDrone.mId.get()<<",isMe:"<<mIsMe<<",mPos:"<<mDrone.mPos.toString()<<",mSpeed:"<<mSpeed.toString();
        return ss.str();
    }
private:
    int lightRange()const{return mIsEnabledPowerLight ? cPowerLightRange : cDarkLightRange;}
    
    Drone mDrone;
    bool mIsMe = true;
    bool mIsEnabledPowerLight = false;
    bool mIsDead = false;
    bool mIsDying = false;
    bool mIsEngineOn = false;
    V2i mSpeed;
};

class SimulatorDroneScan{
public:
    SimulatorDroneScan(const DroneScan& ds, double weight)
    :mDroneScan(ds), mWeight(weight)
    {
        ASSERT(weight>0.0&&weight<=1.0);
    }
    DroneId droneId()const{return mDroneScan.mDroneId;}
    CreatureId creatureId()const{return mDroneScan.mCreatureId;}
//    optional<int> scanningRange() const{return mScanningRange;}
    double getWeight() const{return mWeight;}
    string toString() const{
        stringstream ss;
        ss<<droneId()<<" scan ["<<creatureId()<<"]w:"<<mWeight;
        return ss.str();
    }
    
private:
    DroneScan mDroneScan;
//    optional<int> mScanningRange;
    double mWeight;
};

class Simulator{
public:
    struct Arg{
        Turn mTurn = Turn(0);
        vector<CreatureProfile> mCreatureProfiles;
        const GameScorer* mpScorer;
        Score mMyScore = Score(0);
        Score mFoeScore = Score(0);
        vector<CreatureId> mMyScans;
        vector<CreatureId> mFoeScans;
        vector<Drone> mMyDrones;
        vector<Drone> mFoeDrones;
        vector<DroneScan> mDroneScans;
        vector<pair<CreatureId, CreatureMotion>> mCreatureMotions;
    };
    
public:
    Simulator(const Arg& arg)
    :mTurn(arg.mTurn)
    ,mScorer(*arg.mpScorer)
    {
        for(const DroneScan& ds: arg.mDroneScans){
            mDroneScans.emplace_back(ds, 1.0); //arg経由は確実にスキャンできているので1.0
        }
        
        for(const CreatureProfile& cp: arg.mCreatureProfiles){
            mCreatures.emplace_back(cp);
        }
        for(const auto& pair: arg.mCreatureMotions){
            FindIf(mCreatures, [&pair](const SimulatorCreature& c){
                return c.id() == pair.first;
            })->setCreatureMotion(pair.second);
        }
        
        mMyPlayer.setScore(arg.mMyScore);
        mMyPlayer.setScans(arg.mMyScans);
        for(const Drone& myDrone: arg.mMyDrones){
            mMyPlayer.updateDrone(myDrone.mId, myDrone.mPos, myDrone.mEmergency, myDrone.mBattery);
        }

        mFoePlayer.setScore(arg.mFoeScore);
        mFoePlayer.setScans(arg.mFoeScans);
        for(const Drone& foeDrone: arg.mFoeDrones){
            mFoePlayer.updateDrone(foeDrone.mId, foeDrone.mPos, foeDrone.mEmergency, foeDrone.mBattery);
        }
    }
    
    void update(const vector<DronesCommand>& commands){
        vector<SimulatorDrone> drones;
        mMyPlayer.forEachDrone([&drones](const Drone& d){drones.emplace_back(d, true);});
        mFoePlayer.forEachDrone([&drones](const Drone& d){drones.emplace_back(d, false);});
        //PowerLightの反映
        for(const DronesCommand& dc: commands){
            FindIf(drones, [&dc](const SimulatorDrone& sd){return sd.id() == dc.mDroneId;})->updatePowerLight(dc.mCommand.isEnabledPowerLight());
        }
        //Batteryの更新
        for(SimulatorDrone& sd: drones){
            sd.updateBattery();
        }
        //ドローンの速度を更新
        for(const DronesCommand& dc: commands){
            FindIf(drones, [&dc](const SimulatorDrone& sd){return sd.id() == dc.mDroneId;})->updateDroneSpeed(dc.mCommand.getMoveTo());
        }
        //ドローンの生存状態を判定
        for(SimulatorDrone& sd: drones){
            if(sd.isFoe()){
                //敵の動きは読めないが、簡単には死なないだろうから何も更新しないことで対処
                continue;
            }
            sd.updateDying(mCreatures);
        }
        //ドローンの位置を更新
        for(SimulatorDrone& sd: drones){
            sd.updateDronePos();
        }
        //前のターンで決まった速度をもとに位置を更新
        //逆に言えば、前のターンの時点で魚が退場するかどうかはもう決まっている
        //魚・モンスターの位置を更新(魚は場外退場処理も)
        for(SimulatorCreature& sc: mCreatures){
            sc.updatePos();
        }
        //モンスターの攻撃対象を更新
        {
            //1.ドローンのスキャン範囲を構築
            vector<DroneScanRange> droneScanRanges;
            for(const SimulatorDrone& sd: drones){
                if(auto dsr = sd.droneScanRange()){
                    droneScanRanges.push_back(*dsr);
                }
            }
            //2.モンスター側の情報更新
            for(SimulatorCreature& sc: mCreatures){
                sc.updateMonsterTarget(droneScanRanges);
            }
        }
        //DroneScanを更新
        {
            for(const SimulatorDrone& d: drones){
                if(d.isDeadOrDying()){
                    //死んでるので無効化
                    EraseIf(mDroneScans, [&d](const SimulatorDroneScan& sds){
                        return sds.droneId() == d.id();
                    });
                }
                for(const SimulatorCreature& sc: mCreatures){
                    if(!sc.isFish()){continue;}
                    if(!sc.getCreatureMotion()){continue;}
                    if(FindIf(mDroneScans, [&sc](const SimulatorDroneScan& sds){return sds.creatureId() == sc.id();})){continue;}
                    if(mScorer.findMyScanTurn(sc.id())){continue;}
                    double scanWeight = d.scanWeight(*sc.getCreatureMotion());
                    if(scanWeight > 0.0){
                        mDroneScans.emplace_back(DroneScan{d.id(), sc.id()}, scanWeight);
                    }
                }
            }
        }

        //スコア更新
        for(const SimulatorDrone& d: drones){
            if(d.isDeadOrDying()){continue;}
            if(!d.isInScoringArea()){continue;}
            for(const SimulatorDroneScan& sds: mDroneScans){
                if(sds.droneId() != d.id()){continue;}
                if(d.isMe()){
                    mScorer.scanByMe(sds.creatureId(), mTurn);
                }else{
                    mScorer.scanByFoe(sds.creatureId(), mTurn);
                }
            }
        }
        mMyPlayer.setScore(mScorer.myScore());
        mFoePlayer.setScore(mScorer.foeScore());

        //DroneScanの中でスキャン済みのものを削除
        EraseIf(mDroneScans, [this](const SimulatorDroneScan& sds){
            return (mScorer.findMyScanTurn(sds.creatureId()) && mMyPlayer.findDrone(sds.droneId())) //自分のDronScanであり、自分のチームによってスキャン済み
                || (mScorer.findFoeScanTurn(sds.creatureId()) && mFoePlayer.findDrone(sds.droneId()));
        });


        //ドローンの復活およびdying->deadの更新
        for(SimulatorDrone& sd: drones){
            sd.updateDead();
        }

        //Fishの状態更新
        vector<V2i> engineOnDronesPosList;
        for(const SimulatorDrone& sd: drones){
            if(sd.isEngineOn()){
                engineOnDronesPosList.push_back(sd.getPos());
            }
        }
        for(SimulatorCreature& creature: mCreatures)
        {
            vector<V2i> otherFishPosList;
            for(const SimulatorCreature& sc: mCreatures){
                if(sc.isFish()
                   && sc.id() != creature.id()
                   && sc.getMotion()
                   ){
                    otherFishPosList.push_back(sc.getMotion()->mPosition);
                }
            }
            creature.updateAsFish(engineOnDronesPosList, otherFishPosList);
        }
        
        //モンスターの速度更新
        for(SimulatorCreature& creature: mCreatures)
        {
            creature.updateAsMonster(mCreatures);
        }
        
        if(shouldFinishGame()){
            //この時点でDroneScan状態の魚はScanできたものとして最終結果を計算するためにScoererを再度更新する
            //どうやらDroneの生死・位置は問わない
            for(const SimulatorDrone& d: drones){
                for(const SimulatorDroneScan& ds: mDroneScans){
                    if(ds.droneId() != d.id()){continue;}
                    if(d.isMe()){
                        mScorer.scanByMe(ds.creatureId(), Turn(mTurn.get()+1));
                    }else{
                        mScorer.scanByFoe(ds.creatureId(), Turn(mTurn.get()+1));
                    }
                }
            }
            mMyPlayer.setScore(mScorer.myScore());
            mFoePlayer.setScore(mScorer.foeScore());
        }

        mTurn += Turn(1);
        
        mSimulatorDrones = drones;
#if 0
        for(const auto& sd: mSimulatorDrones){
            LOG(sd.toString() << endl);
        }
#endif
    }
    
    Score getMyScore()const{return mMyPlayer.getScore();}
    Score getFoeScore()const{return mFoePlayer.getScore();}

    //どちらのDroneがDroneScanしたかを区別しないときのDroneScan済みリスト
    vector<CreatureId> myDroneScanCreatureIds() const{
        vector<CreatureId> ret;
        forEachDroneScan_(mMyPlayer, [&ret](const SimulatorDroneScan& sds){
            if(FindIf(ret, [&sds](const CreatureId& id){return id == sds.creatureId();})){
                return;
            }
            ret.push_back(sds.creatureId());
        });
        return ret;
    }
    
    
    const SimulatorDroneScan* findMyDroneScan(CreatureId id)const{
        return findDroneScan_(mMyPlayer, id);
    }
    const SimulatorDroneScan* findFoeDroneScan(CreatureId id)const{
        return findDroneScan_(mFoePlayer, id);
    }

    const SimulatorDrone& myFirstSimulatorDrone() const{
        return *findMySimulatorDrone_(0);
    }
    const SimulatorDrone& mySecondSimulatorDrone() const{
        return *findMySimulatorDrone_(1);
    }
    template<typename Func>
    void forEachMySimulatorDrone(Func&& func) const{
        for(const SimulatorDrone& sd: mSimulatorDrones){
            if(sd.isMe()){func(sd);}
        }
    }
    template<typename Func>
    void forEachSimulatorCreature(Func&& func) const{
        for(const SimulatorCreature& sc: mCreatures){
            func(sc);
        }
    }
    const SimulatorCreature* findSimulatorCreature(CreatureId id)const{
        for(const SimulatorCreature& sc: mCreatures){
            if(sc.id() == id){return &sc;}
        }
        return nullptr;
    }
    optional<Turn> findMyScanTurn(CreatureId id) const{
        return mScorer.findMyScanTurn(id);
    }
    optional<Turn> findFoeScanTurn(CreatureId id) const{
        return mScorer.findFoeScanTurn(id);
    }
    template<typename Func>
    void forEachMyDroneScan(Func&& func) const{
        return forEachDroneScan_(mMyPlayer, func);
    }
    template<typename Func>
    void forEachFoeDroneScan(Func&& func) const{
        return forEachDroneScan_(mFoePlayer, func);
    }

private:
    template<typename Func>
    const SimulatorDroneScan* findDroneScanIf_(const Player& player, Func&& func) const{
        for(const SimulatorDroneScan& sds: mDroneScans){
            if(!player.findDrone(sds.droneId())){continue;}
            if(func(sds)){return &sds;}
        }
        return nullptr;
    }
    template<typename Func>
    void forEachDroneScan_(const Player& player, Func&& func) const{
        findDroneScanIf_(player, [&func](const SimulatorDroneScan& sds){func(sds);return false;});
    }
    const SimulatorDroneScan* findDroneScan_(const Player& player, CreatureId id) const{
        return findDroneScanIf_(player, [id](const SimulatorDroneScan& sds){return sds.creatureId() == id;});
    }
    

    const SimulatorDrone* findMySimulatorDrone_(int nth) const{
        int count = 0;
        for(const SimulatorDrone& sd: mSimulatorDrones){
            if(sd.isMe()){
                if(count++ == nth){return &sd;}
            }
        }
        ASSERT(false);
        return nullptr;
    };
    
    bool shouldFinishGame() const{
        if(hasScannedAllRemainingFish(mMyPlayer) || hasScannedAllRemainingFish(mFoePlayer)){
            return true;
        }
        if(mTurn >= cGameEndTurn){
            return true;
        }
        if(mMyPlayer.getScore() > computeMaxPlayerScore(mFoePlayer)){
            return true;
        }
        if(mFoePlayer.getScore() > computeMaxPlayerScore(mMyPlayer)){
            return true;
        }
        return false;
    }
    bool hasScannedAllRemainingFish(const Player& player) const{
        for(const SimulatorCreature& sc: mCreatures){
            if(sc.isMonster() || sc.isLostFish()){continue;}
            if(!player.containsScaned(sc.id())){
                return false;
            }
        }
        return true;
    }
    Score computeMaxPlayerScore(const Player& player)const{
        GameScorer scorer = mScorer;
        //Playerにとって未Scanの魚を次のターンでスキャンできたとすれば最大値
        for(const SimulatorCreature& sc: mCreatures){
            if(sc.isMonster() || sc.isLostFish()){continue;}
            if(!player.containsScaned(sc.id())){
                if(&player == &mMyPlayer){
                    scorer.scanByMe(sc.id(), Turn(mTurn.get()+1));
                } else {
                    scorer.scanByFoe(sc.id(), Turn(mTurn.get()+1));
                }
            }
        }
        if(&player == &mMyPlayer){
            return scorer.myScore();
        } else {
            return scorer.foeScore();
        }
    }
    
    vector<CreatureProfile> mProfiles;
    Turn mTurn;
    Player mMyPlayer;
    Player mFoePlayer;
    vector<SimulatorDroneScan> mDroneScans;
    vector<SimulatorCreature> mCreatures;
    vector<SimulatorDrone> mSimulatorDrones;
    GameScorer mScorer;
};

template<typename Func>
void ForEachAllCommands(V2i basePos, Battery battery, Func&& func){
    Command waitCommand;
    func(waitCommand);

    if(battery >= cPowerLightBattery){
        waitCommand.enablePowerLight();
        func(waitCommand);
    }

    //分割するほど重くなるので注意！
    vector<V2i> dirs;
    constexpr int cDiv = 32;
    for(int i=0;i<cDiv;i++){
        const double r = 2 * M_PI * i / cDiv;
        dirs.push_back({(int)(1000*cos(r)), (int)(1000*sin(r))});
    }
    
    for(const V2i& way:dirs){
        Command moveCommand;
        moveCommand.setMove(basePos + way);
        func(moveCommand);
        
        if(battery >= cPowerLightBattery){
            moveCommand.enablePowerLight();
            func(moveCommand);
        }
    }
}

vector<pair<Command, Command>> AllCommandPairs(const V2i& basePos0, Battery battery0, const V2i& basePos1, Battery battery1){
    vector<pair<Command, Command>> ret;
    ForEachAllCommands(basePos0, battery0, [&basePos1,&battery1,&ret](const Command& cmd0){
        ForEachAllCommands(basePos1, battery1, [&cmd0, &ret](const Command& cmd1){
            ret.emplace_back(cmd0, cmd1);
        });
    });
    return ret;
}

class VictoryCondition{
public:
    enum class Type{
        None,
        Win,
        Scan,
        UpperFish,
        Other
    };
    
    VictoryCondition(const PlayField& pf){
        //0.現時点での自分のスコア
        //1.自分のDroneScanを全部スコア化
        //2.自分の上方にいる魚を全部スコア化
        //3.相手がDroneScanおよびLostしていないすべてのFishをスコア化
        //4.自分が残りをスコア化
        //この順でスコア化したと比較して3をしたときの敵のスコアの大小関係によっては網羅的なDroneScanを省略して勝ちに向かうことができる

        //(A) < 現時点 < (B) < 自分のDroneScanを全部 < (C) < 自分のDroneScan全部+上方の魚 < (D) < 自分にとっての残りの全魚 < (E)
        //それぞれの位置で
        //(A) ... 引き分け以上が保証されている。自分は残りの魚をのんびり探せばいい
        //(B) ... 相手の何らかのスコア化よりも先に今のDroneScanをスコア化すれば勝てる
        //(C) ... 相手の何らかのスコア化よりも先に上方の魚をすべてDroneScan&スコア化すれば勝てる
        //(D) ... 今後の自分のDroneScanが速やかに進めば勝てる可能性がある
        //(E) ... 相手がモンスターと衝突するとかすれば勝てるかも

        //この順番で、なお引き分け以上なら勝ち確定を試したい
        Turn turn = pf.getTurn();
        GameScorer scorer(pf.getScorer());
        
        //0.今のスコア
        Score myCurScore = scorer.myScore();
        
        //1.自分のDroneScanを全部スコア化
        vector<DroneScan> myDroneScans;
        pf.forEachMyDroneScan([&myDroneScans](const DroneScan& ds){
            myDroneScans.push_back(ds);
        });
        for(const DroneScan& ds: myDroneScans){
            scorer.scanByMe(ds.mCreatureId, turn);
        }
        Score myDSScore = scorer.myScore();
        //2.自分の上方の魚を全部スコア化(3との時間的な前後関係が厳密ではない気がするが...?)
        turn += Turn(1);
        int basePosY = max(pf.getMyDrone(0).mPos.y(), pf.getMyDrone(1).mPos.y()) + 300;
        pf.forEachActiveFish([basePosY, &scorer, &turn, this](const Creature& c){
            if(c.bestEffortPos()->y() <= basePosY && !scorer.findMyScanTurn(c.id()) ){
                mUpperFishCreatureIds.push_back(c.id());
                scorer.scanByMe(c.id(), turn);
            }
        });
        Score myUpperScanScore = scorer.myScore();
        //3.相手がLostしていないすべてのFishをスコア化
        // turn += Turn(1); ... これはどうする...?
        pf.forEachFoeDroneScan([&scorer, &turn](const DroneScan& ds){
            scorer.scanByFoe(ds.mCreatureId, turn);
        });
        turn += Turn(1);
        pf.forEachActiveFish([&scorer, turn](const Creature& c){
            if(!scorer.findFoeScanTurn(c.id())){
                scorer.scanByFoe(c.id(), turn);
            }
        });
        Score foeScore = scorer.foeScore();
        //4.自分が残りをスコア化
        pf.forEachActiveFish([&scorer, turn](const Creature& c){
            if(!scorer.findMyScanTurn(c.id())){
                scorer.scanByMe(c.id(), turn);
            }
        });
        Score myScore = scorer.myScore();
        LOG("[CetainScore]foeScore:"<<foeScore<<",myCurScore:"<<myCurScore<<",DSScore:"<<myDSScore<<",UpperScore:"<<myUpperScanScore<<",myScore:"<<myScore<<endl);
        if(foeScore <= myCurScore){
            //ほとんど負けようがない、残りを探せば良い
            LOG("Winning Run myScore:"<<myScore<<",forScore:"<<foeScore<<endl);
            mType = Type::Win;
            return;
        }
        if(foeScore <= myDSScore){
            LOG("VictoryScan myScore:"<<myScore<<",forScore:"<<foeScore<<endl);
            mType = Type::Scan;
            return;
        }
        if(foeScore <= myUpperScanScore){
            LOG("VictoryUpperScan myScore:"<<myScore<<",forScore:"<<foeScore<<endl);
            mType = Type::UpperFish;
            return;
        }
        if(foeScore <= myScore){
            LOG("Remain Victory myScore:"<<myScore<<",forScore:"<<foeScore<<endl);
            mType = Type::Other;
            return;
        }
        LOG("Not Certain myScore:"<<myScore<<",forScore:"<<foeScore);
        mType = Type::None;
    }

    bool needScan() const{return mType == Type::Scan;}
    bool needScanUpperFish() const{return mType == Type::UpperFish;}
    bool needNormal() const{return mType == Type::Win || mType == Type::Other || mType == Type::None;}
    const vector<CreatureId>& getUpperFishCreatureIds() const{return mUpperFishCreatureIds;}
private:
    
    Type mType = Type::None;
    vector<CreatureId> mUpperFishCreatureIds;
};

enum CertainVictory{
    None, Scan, Remain
};

class EvaluatedDroneScore{
public:
    EvaluatedDroneScore(const SimulatorDrone* pSimDrone):mpSimDrone(pSimDrone){}
    
    DroneId id() const{return mpSimDrone->id();}

    void setNearestUnscannedFish(CreatureId id, int distance){
        mNearestFishId = id;
        mNearestFishDistance = distance;
    }
    
    double score() const{
        return -mNearestFishDistance
        - (mpSimDrone->isDeadOrDying() ? 1000000.0 : 0)
        - (mpSimDrone->getPos().y() * 0.001)
        + sqrt((mpSimDrone->getBattery().get())/30.0)
        - ((mpSimDrone->isBatteryFull() && mpSimDrone->getPos().y() > cMonsterHavitatTop) ? 10000.0 : 0)
        ;
    }
    double resurfaceScore()const{
        return
        - (mpSimDrone->isDeadOrDying() ? 1000000.0 : 0)
        - (mpSimDrone->getPos().y())
        - (mpSimDrone->isBatteryFull() ? 10000.0 : 0)
        ;
    }
    
    int y()const{return mpSimDrone->getPos().y();}
    Battery battery()const{return mpSimDrone->getBattery();}
    
    string toStr() const{
        stringstream ss;
        ss << "[" << mpSimDrone->id() << "] nearest:[" <<mNearestFishId.get()<<"]"<<mNearestFishDistance<<"u,dead:"<<(mpSimDrone->isDeadOrDying()?1:0);
        return ss.str();
    }
    
    

private:
    const SimulatorDrone* mpSimDrone;
    CreatureId mNearestFishId = CreatureId(-1);
    int mNearestFishDistance = 0;
};


class EvaluatedScore{
public:
    EvaluatedScore(const SimulatorDrone* pSimDrone0, const SimulatorDrone* pSimDrone1, const VictoryCondition& condition)
    :mDroneScore0(pSimDrone0)
    ,mDroneScore1(pSimDrone1)
    ,mpCondition(&condition)
    {}

    void setNearestUnscannedFish(DroneId droneId, CreatureId id, int distance){
        findDroneScore_(droneId)->setNearestUnscannedFish(id, distance);
    }
    void setScore(Score score){mScore = score;}
    void setDroneScanNum(double num){mDroneScanNum = num;}
    void setMonopolyFishNum(double myNum, double foeNum){
        mMyMonopolyFishNum = myNum;
        mFoeMonopolyFishNum = foeNum;
    }
#if 0
    void setNewScanningRangeSum(int rangeSum){
        mNewScannningRangeSum = rangeSum;
    }
#endif
    
    double score() const{
        if(mpCondition->needScan()){
            return mScore.get()*10000
            + mDroneScore0.resurfaceScore()
            + mDroneScore1.resurfaceScore();
        }
        
        return mScore.get()*10000
        + (mMyMonopolyFishNum - mFoeMonopolyFishNum)*100 
        + mDroneScanNum * 10
//        + mNewScannningRangeSum * 0.001
        + (mDroneScore0.score() + mDroneScore1.score())*0.00001;
    }

    string toStr() const{
        stringstream ss;
        ss<<mScore.get()<<",DS:"<<mDroneScanNum<<","<<mDroneScore0.toStr()<<"|"<<mDroneScore1.toStr()<<"mono:("<<mMyMonopolyFishNum<<","<<mFoeMonopolyFishNum<<")";
        return ss.str();
    }
    
private:
    EvaluatedDroneScore* findDroneScore_(DroneId id){return mDroneScore0.id() == id ? &mDroneScore0 : &mDroneScore1;}

    Score mScore = Score(0);
    const VictoryCondition* mpCondition;
    double mDroneScanNum = 0;
    double mMyMonopolyFishNum = 0;
    double mFoeMonopolyFishNum = 0;
//    int mNewScannningRangeSum = 0;
    EvaluatedDroneScore mDroneScore0;
    EvaluatedDroneScore mDroneScore1;
};

Simulator::Arg PlayFieldSimulatorArg(const PlayField& pf){
    Simulator::Arg arg;
    arg.mTurn = pf.getTurn();
    arg.mCreatureProfiles = pf.getCreatureProfiles();
    arg.mpScorer          = &pf.getScorer();
    arg.mMyScore          = pf.getMyScore();
    arg.mFoeScore         = pf.getFoeScore();
    arg.mMyScans          = pf.getMyScans();
    arg.mFoeScans         = pf.getFoeScans();
    arg.mMyDrones         = pf.getMyDrones();
    arg.mFoeDrones        = pf.getFoeDrones();
    arg.mDroneScans       = pf.getDroneScans();
    
    pf.forEachCreature([&arg](const Creature& creature){
        if(auto visibleMotion = creature.visibleMotion()){
            arg.mCreatureMotions.push_back({creature.id(), CreatureMotion(*visibleMotion)});
        } else if(const auto& estimatedMotion = creature.estimatedMotion()){
            arg.mCreatureMotions.push_back({creature.id(), CreatureMotion(*estimatedMotion)});
        }
    });
    return arg;
}

class DroneScanTargetter{
public:
    struct FishEval{
        CreatureId mId;
        V2i mPos;
        int mDrone0Distance;
        int mDrone1Distance;
    };
public:
    DroneScanTargetter(const Simulator& sim){
        //未スキャン・未DroneScan・位置が推定済み(Lostしてない)のFishのみを取り出してvectorに詰める
        
        sim.forEachSimulatorCreature([this, &sim](const SimulatorCreature& sc){
            auto motion = sc.getMotion();
            if(!sc.isFish() || sc.isLostFish() || !motion){ return;}
            if(sim.findMyDroneScan(sc.id()) || sim.findMyScanTurn(sc.id())){
                return;
            }
            mFishes.push_back({sc.id()
                , motion->mPosition
                , (motion->mPosition - sim.myFirstSimulatorDrone().getPos()).length()
                , (motion->mPosition - sim.mySecondSimulatorDrone().getPos()).length()
            });
        });
    }
    DroneScanTargetter(const PlayField& pf, const VictoryCondition& condition){
        //未スキャン・未DroneScan・位置が推定済み(Lostしてない)のFishのみを取り出してvectorに詰める
        pf.forEachCreature([&pf, this](const Creature& c){
            if(!c.isFish()){return;}
            if(!c.isScanTypeNoneForMe()){return;}
            auto pos = c.bestEffortPos();
            if(!pos){return;}
            mFishes.push_back({c.id()
                , *pos
                , (*pos - pf.getMyDrone(0).mPos).length()
                , (*pos - pf.getMyDrone(1).mPos).length()
            });
        });
        
        vector<const FishEval*> sortedList; sortedList.reserve(mFishes.size());
        if(condition.needScanUpperFish()){
            for(const CreatureId id: condition.getUpperFishCreatureIds()){
                sortedList.push_back(FindIf(mFishes, [id](const FishEval& fish){
                    return fish.mId == id;
                }));
            }
        }else{
            for(const auto& fish: mFishes){
                sortedList.push_back(&fish);
            }
        }

        //drone0とdrone1の距離差を見て、よりdrone0に近いものが先に並ぶようにソート
        sort(sortedList.begin(), sortedList.end(), [](const FishEval* f0, const FishEval* f1){
            return (f0->mDrone1Distance - f0->mDrone0Distance) > (f1->mDrone1Distance - f1->mDrone0Distance);
        });
        
        const int half = sortedList.size()/2;
        for(int i=0;i<sortedList.size();i++){
            const FishEval* pFish = sortedList[i];
            if(pFish->mDrone0Distance < 400){
                mDrone0Fishes.push_back(pFish);
                continue;
            } else if(pFish->mDrone1Distance < 400){
                mDrone1Fishes.push_back(pFish);
                continue;
            }
            if(i < half){
                mDrone0Fishes.push_back(pFish);
            }else{
                mDrone1Fishes.push_back(pFish);
            }
        }
    }
    
    optional<FishEval> firstDroneTarget() const{
        double bestEval = numeric_limits<double>::infinity();
        optional<FishEval> ret;
        for(const FishEval* pFish: mDrone0Fishes){
            const auto& fish = *pFish;
            //自分の担当とみなして評価。小さい方が有利とする。
            double s = fish.mDrone0Distance * 0.3 + (cFieldBottom - fish.mPos.y()) * 0.7;
            if(s < bestEval){
                ret = fish;
                bestEval = s;
            }
        }
        return ret;
    }
    optional<FishEval> secondDroneTarget() const{
        double bestEval = numeric_limits<double>::infinity();
        optional<FishEval> ret;
        for(const FishEval* pFish: mDrone1Fishes){
            const auto& fish = *pFish;
            //自分の担当とみなして評価。小さい方が有利とする。
            double s = fish.mDrone1Distance * 0.3 + (cFieldBottom - fish.mPos.y()) * 0.7;
            if(s < bestEval){
                ret = fish;
                bestEval = s;
            }
        }
        return ret;

    }
    vector<FishEval> mFishes;
    vector<const FishEval*> mDrone0Fishes;
    vector<const FishEval*> mDrone1Fishes;
};

//もう片方のDroneの動きを決め打ちにして、候補となるコマンドのなかから最もいい評価のコマンドを選ぶ
class SingleMoveCommandEvaluator{
public:
    SingleMoveCommandEvaluator(const PlayField& pf, const optional<Command>& firstCommand, const optional<Command>& secondCommand, const VictoryCondition& condition)
    :mSimulator(PlayFieldSimulatorArg(pf))
    {
        //どちらかのコマンドは有効、どちらかは無効。無効の方を評価対象とする。
        ASSERT(firstCommand||secondCommand);
        ASSERT(!firstCommand||!secondCommand);
        
        vector<DronesCommand> commands;
        
        if(firstCommand){
            eval(pf, pf.getMyDrone(1), DronesCommand{pf.getMyDrone(0).mId, *firstCommand}, condition);
        } else if(secondCommand){
            eval(pf, pf.getMyDrone(0), DronesCommand{pf.getMyDrone(1).mId, *secondCommand}, condition);
        }
    }
    
    //評価対象のDroneを入れる
    void eval(const PlayField& pf, const Drone& drone, const DronesCommand& otherCommand, const VictoryCondition& condition){
        double bestEval = -numeric_limits<double>::infinity();
        DronesCommand bestCommand;
        DroneScanTargetter targetter(pf, condition);
        
        ForEachAllCommands(drone.mPos, drone.mBattery, [&drone,&pf,&otherCommand,&bestEval,&bestCommand,&targetter,this,&condition](const Command& command){
            Simulator sim(PlayFieldSimulatorArg(pf));
            DronesCommand cmd{drone.mId, command};
            sim.update({cmd, otherCommand});
      
            EvaluatedScore evaluated(&sim.myFirstSimulatorDrone(), &sim.mySecondSimulatorDrone(), condition);
            evaluated.setScore(sim.getMyScore());
            double weightSum = 0.0;
            sim.forEachMyDroneScan([&weightSum](const SimulatorDroneScan& sds){
//                weightSum += round(sds.getWeight());
                weightSum += sds.getWeight();
            });
            evaluated.setDroneScanNum(weightSum);
            vector<CreatureId> myMonopolyList;
            vector<CreatureId> foeMonopolyList;
//            int myMonopolyEstimatedTurnSum = 0;
            sim.forEachSimulatorCreature([&myMonopolyList,&foeMonopolyList,&sim](const SimulatorCreature& sc){
//                if(!sc.isLostFish() && !sc.willBeLostFish()){return;}
                if(sc.isMonster()){return;}
                bool isMyScanned = sim.findMyScanTurn(sc.id()) || sim.findMyDroneScan(sc.id());
                bool isFoeScanned = sim.findFoeScanTurn(sc.id()) || sim.findFoeDroneScan(sc.id());
                if(isMyScanned && !isFoeScanned){
                    if(sc.isLostFish() || sc.willBeLostFish()){
                        myMonopolyList.push_back(sc.id());
                    } else if(auto estimated = sc.fishLostEstimatedTurn()){
//                        myMonopolyCount+=pow(0.7,*estimated);
                    }
                } else if(isFoeScanned && !isMyScanned){
                    if(sc.isLostFish() || sc.willBeLostFish()){
                        foeMonopolyList.push_back(sc.id());
                    }
                }
            });
            evaluated.setMonopolyFishNum(myMonopolyList.size(), foeMonopolyList.size());

            sim.forEachMySimulatorDrone([&sim,&evaluated,&targetter](const SimulatorDrone& sd){
                if(sd.id().get() < 2){
                    if(auto fish = targetter.firstDroneTarget()){
                        evaluated.setNearestUnscannedFish(sd.id(), fish->mId, (sim.findSimulatorCreature(fish->mId)->getMotion()->mPosition - sd.getPos()).length() );
                    }
                } else {
                    if(auto fish = targetter.secondDroneTarget()){
                        evaluated.setNearestUnscannedFish(sd.id(), fish->mId, (sim.findSimulatorCreature(fish->mId)->getMotion()->mPosition - sd.getPos()).length());
                    }
                }
            });
            double s = evaluated.score();
            if(s > bestEval){
                bestEval = s;
                bestCommand = cmd;
                mBestCommandResult = evaluated.toStr();
            }
        });
        mBestCommand = bestCommand;
    };
    
    const DronesCommand& getBestCommand()const{return mBestCommand;}
    
    string getBestCommandResult()const{
        return mBestCommandResult;
    }
    
    DroneId getDroneId()const{return mBestCommand.mDroneId;}
    
private:
    Simulator mSimulator;
    DronesCommand mBestCommand;
    string mBestCommandResult;
};

void RunTest();

int main(int argc, const char * argv[]) {
#if DEBUG
    RunTest();
#endif
    
    PlayFieldUpdater updater(cin);
    while(true){
        const PlayField& pf = updater.update();
        VictoryCondition condition(pf);

        //組み合わせて評価したいが、お互いの動きが関係しない場面も多くいため
        //1)1がWAITの前提で0を動かす
        //2)0がその動きをする前提で1を動かす
        //という2段階で評価する
        //2つのDroneが近いときは別の考え方が必要かもしれないが、独立しているときはこれで十分動くはず
        SingleMoveCommandEvaluator eval0(pf, nullopt, Command{}, condition);
        SingleMoveCommandEvaluator eval1(pf, eval0.getBestCommand().mCommand, nullopt, condition);
        
        LOG(eval1.getBestCommandResult()<<endl);
        
        //確認とフィードバック用
        Simulator sim(PlayFieldSimulatorArg(pf));
        sim.update({eval0.getBestCommand(), eval1.getBestCommand()});
#if DEBUG
        sim.forEachSimulatorCreature([](const SimulatorCreature& sc){
            LOG(sc.toString() << endl);
        });
        sim.forEachMyDroneScan([](const SimulatorDroneScan& sds){
            LOG(sds.toString() << endl);
        });
#endif
        
        eval0.getBestCommand().out(cout);
        eval1.getBestCommand().out(cout);
            
        if(eval0.getBestCommand().isEnabledPowerLight()){
            updater.recordPowerLight(eval0.getDroneId());
        }
        if(eval1.getBestCommand().isEnabledPowerLight()){
            updater.recordPowerLight(eval1.getDroneId());
        }
        sim.forEachSimulatorCreature([&updater](const SimulatorCreature& sc){
            //確実な位置情報からのシミュレート結果は、次のターンにも活かす
            if(sc.getCreatureMotion() && !sc.getCreatureMotion()->getEstimatedArea()){
                updater.addSimulatedPos(sc.id(), *sc.getMotion());
            }
        });
    }

    return 0;
}

void RunTest(){
    ASSERT(!DroneAndMonsterCollide({2864,6694}, {600,0}, {2111, 6480}, {540,0}));
    ASSERT(DroneAndMonsterCollide({7768,7016}, {0,300}, {7318, 7704}, {220,-156}));
    {
        //でっち上げる
        vector<CreatureProfile> profiles;
        for(int i=0;i<12;i++){
            profiles.push_back({CreatureId(i), CreatureType(i/4), CreatureColor(i%4)});
        }
        {
            GameScorer scorer(profiles);
            for(int i=0;i<12;i++){
                scorer.scanByMe(CreatureId(i), Turn(0));
            }
            Score s = scorer.myScore();
            ASSERT(s == Score(96));
        }
        {
            GameScorer scorer(profiles);
            scorer.scanByFoe(CreatureId(0), Turn(0));
            scorer.scanByFoe(CreatureId(1), Turn(0));
            scorer.scanByFoe(CreatureId(2), Turn(0));
            scorer.scanByFoe(CreatureId(3), Turn(0));
            scorer.scanByFoe(CreatureId(4), Turn(0));
            scorer.scanByFoe(CreatureId(5), Turn(0));
            scorer.scanByFoe(CreatureId(6), Turn(0));
            scorer.scanByFoe(CreatureId(7), Turn(0));
            scorer.scanByFoe(CreatureId(8), Turn(0));
            scorer.scanByFoe(CreatureId(9), Turn(0));
            for(int i=0;i<12;i++){
                scorer.scanByMe(CreatureId(i), Turn(1));
            }

            Score s = scorer.myScore();
            ASSERT(s == Score(64));
        }
    }
    
}
